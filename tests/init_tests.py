import unittest
from modules.open_digraph import OpenDigraph
from modules.node import Node


class InitTest(unittest.TestCase):
    def test_init_node(self):
        n0 = Node('i', 0, {}, {1: 1})
        self.assertEqual(n0.label, 'i')
        self.assertEqual(n0.id, 0)
        self.assertEqual(n0.parents_ids, {})
        self.assertEqual(n0.children_ids, {1: 1})
        self.assertIsInstance(n0, Node)

    def test_init_open_digraph(self):
        n0 = Node('i', 0, {}, {1: 1})
        od = OpenDigraph([1], [2], [n0])
        self.assertEqual(od.inputs_ids, [1])
        self.assertEqual(od.outputs_ids, [2])
        self.assertEqual(od.nodes, [n0])
        self.assertIsInstance(od, OpenDigraph)
