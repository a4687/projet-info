from random import choice
import unittest
from modules.open_digraph import OpenDigraph, DigraphType
from modules.node import Node


class OpenDigraphTest(unittest.TestCase):
    def setUp(self):
        self.d = OpenDigraph([], [], [Node('A', 1, {}, {2: 1, 4: 1, 5: 1}),
                                      Node('B', 2, {1: 1}, {3: 1}),
                                      Node('C', 3, {2: 1, 4: 1}, {}),
                                      Node('D', 4, {1: 1}, {3: 1}),
                                      Node('E', 5, {1: 1}, {})])

    def test_well_formed_good(self):
        # D is actually a good digraph
        self.assertTrue(self.d.is_well_formed())

    def test_add_node(self):
        self.d.add_node('F', {5: 1, 1: 2}, {1: 1, 3: 1})
        self.assertTrue(self.d.is_well_formed())

    def test_remove_node(self):
        self.d.remove_node_by_id(1)
        self.assertTrue(self.d.is_well_formed())

    def test_add_edge(self):
        self.d.add_edge(1, 3)
        self.assertTrue(self.d.is_well_formed())

    def test_remove_edge(self):
        self.d.remove_edge(1, 2)
        self.assertTrue(self.d.is_well_formed())

    def test_add_output_node(self):
        self.d.add_output_node(1)
        self.assertTrue(self.d.is_well_formed())

    def test_add_input_node(self):
        self.d.add_input_node(1)
        self.assertTrue(self.d.is_well_formed())

    def test_wf_input_not_in_digraph(self):
        # Input node not in digraph
        self.d.inputs_ids += [-8]
        self.assertFalse(self.d.is_well_formed())

    def test_wf_output_not_in_digraph(self):
        # Output node not in digraph
        self.d.outputs_ids += [-38]
        self.assertFalse(self.d.is_well_formed())

    def test_wf_input_with_children(self):
        # Input node with more than one child
        self.d.add_input_node(choice(self.d.nodes_ids))
        self.d.add_edge(self.d.inputs_ids[0], choice(self.d.nodes_ids))
        self.assertFalse(self.d.is_well_formed())

    def test_wf_output_with_parents(self):
        # Output node with more than one parent
        self.d.add_output_node(choice(self.d.nodes_ids))
        self.d.add_edge(choice(self.d.nodes_ids), self.d.outputs_ids[0])
        self.assertFalse(self.d.is_well_formed())

    def test_wf_input_multiplicity_superior(self):
        # Input node with a multiplicity > 1
        child_node = choice(self.d.nodes_ids)
        self.d.add_input_node(child_node)
        self.d.add_edge(self.d.inputs_ids[0], child_node)
        self.assertFalse(self.d.is_well_formed())

    def test_wf_output_multiplicity_superior(self):
        # Output node with a multiplicity > 1
        parent_node = choice(self.d.nodes_ids)
        self.d.add_output_node(parent_node)
        self.d.add_edge(parent_node, self.d.outputs_ids[0])
        self.assertFalse(self.d.is_well_formed())

    def test_wf_input_with_parent(self):
        # Input node with a parent
        self.d.add_input_node(choice(self.d.nodes_ids))
        self.d.add_node('N', {self.d.inputs_ids[0]: 1})
        self.assertFalse(self.d.is_well_formed())

    def test_wf_output_with_child(self):
        # Output node with a child
        self.d.add_output_node(choice(self.d.nodes_ids))
        self.d.add_node('N', {self.d.outputs_ids[0]: 1})
        self.assertFalse(self.d.is_well_formed())

    def test_wf_key_not_node_id(self):
        # key of nodes_map not a real id
        self.d.node_map[-5] = None
        self.assertFalse(self.d.is_well_formed())

        # key of nodes_map != id of associated node
        self.d.node_map[-5] = self.d.node_map[choice(self.d.nodes_ids)].copy()
        self.assertFalse(self.d.is_well_formed())

    def test_wf_wrong_family_links(self):
        # j has i as child but i doesn't have j as parent
        self.d.node_map[5].add_child_id(3)
        self.assertFalse(self.d.is_well_formed())

        # j has i as parent but i doesn't have j as child
        self.d.node_map[5].add_parent_id(3)
        self.assertFalse(self.d.is_well_formed())

    def test_wf_wrong_family_multiplicity(self):
        # j has i as child but i doesn't have j as parent with same multiplicity (2)
        self.d.add_edge(3, 5)
        self.d.node_map[5].add_child_id(3)
        self.assertFalse(self.d.is_well_formed())

        # j has i as parent but i doesn't have j as child with same multiplicity (2)
        self.d.add_edge(3, 5)
        self.d.node_map[5].add_parent_id(3)
        self.assertFalse(self.d.is_well_formed())

    def test_shift_indices(self):
        min_id = self.d.min_id()
        max_id = self.d.max_id()

        n = choice(range(-10, 11))
        self.d.shift_indices(n)

        self.assertEqual(min_id + n, self.d.min_id())
        self.assertEqual(max_id + n, self.d.max_id())
        self.assertTrue(self.d.is_well_formed())

    def test_parallel(self):
        a_number = 20

        d_list = []
        while len(d_list) < a_number:
            i = len(d_list) + 1
            tmp_digraph = OpenDigraph.random(i, 10, DigraphType.LoopFree, i, a_number - i)
            con_nb, _ = tmp_digraph.connected_components()
            if con_nb == 1:
                d_list += [tmp_digraph]

        tmp = OpenDigraph.parallel(d_list)
        self.assertTrue(tmp.is_well_formed())

        tmp_list = []

        extracted_list = tmp.connected_components_list()
        self.assertEqual(len(extracted_list), a_number)

        for digraph in extracted_list:
            self.assertEqual(a_number, len(digraph.inputs_ids) + len(digraph.outputs_ids))
            i = len(digraph.inputs_ids)
            self.assertEqual(i + a_number, len(digraph.nodes))
            self.assertNotIn(i, tmp_list)
            self.assertTrue(digraph.is_well_formed())
            tmp_list += [i]

    def test_dijkstra(self):
        dist, prev = self.d.dijkstra(1)
        self.assertEqual(0, dist[1])
        self.assertEqual(2, dist[3])
        self.d.add_edge(1, 3)
        dist, prev = self.d.dijkstra(1)
        self.assertEqual(1, dist[3])

        new_id = self.d.add_node("Coucou")
        dist, prev = self.d.dijkstra(1)
        self.assertNotIn(new_id, dist)

        self.d.add_edge(new_id, 2)

        dist, prev = self.d.dijkstra(3, direction=1)
        self.assertNotIn(new_id, dist)

        dist, prev = self.d.dijkstra(3, direction=-1)
        self.assertIn(new_id, dist)
        self.assertEqual(2, dist[new_id])
