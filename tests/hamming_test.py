import unittest
from random import sample

from modules.bool_circ import BoolCirc, BoolNodeTypes


class HammingTest(unittest.TestCase):
    @staticmethod
    def build_graph(error_position=[]):
        enc = BoolCirc.hamming_encoder()
        dec = BoolCirc.hamming_decoder()

        output_enc = [node_id + enc.parallel_shift(dec) for node_id in enc.outputs_ids]
        enc.iparallel(dec)  # dec ids aren't changed, yay !

        for k, enc_output_id in enumerate(output_enc):
            enc_var, enc_var_pos = enc.decompose_node_name(enc_output_id)

            if enc_var == 'o':
                target_node = dec.get_var_list('i', get_input=True)[enc_var_pos]
            else:
                target_node = dec.get_var_list('r', get_input=True)[enc_var_pos]

            output_parent_id = enc.get_node_by_id(enc_output_id).parents[0]
            input_child_id = target_node.children[0]

            if k in error_position:
                not_node = enc.add_node(BoolNodeTypes.NOT)
                enc.add_edge(output_parent_id, not_node)
                enc.add_edge(not_node, input_child_id)
            else:
                enc.add_edge(output_parent_id, input_child_id)

            enc.remove_several_node_by_id([enc_output_id, target_node.id])

        return enc

    @staticmethod
    def is_identity(graph: BoolCirc):
        if not graph.is_well_formed():
            return False

        for input_id in graph.inputs_ids:
            in_var, in_var_num = graph.decompose_node_name(input_id)

            child_id = graph.get_node_by_id(input_id).children[0]
            if child_id not in graph.outputs_ids:
                return False

            out_var, out_var_num = graph.decompose_node_name(child_id)

            if in_var_num != out_var_num:
                return False

        return True

    def test_with_no_error(self):
        graph = HammingTest.build_graph()
        graph.simplify()
        self.assertTrue(HammingTest.is_identity(graph))

    def test_with_one_error(self):
        for error_position in range(7):
            graph = HammingTest.build_graph([error_position])
            graph.simplify()
            self.assertTrue(HammingTest.is_identity(graph))

    def test_with_more_error(self):
        for error_number in range(2, 7):
            graph = HammingTest.build_graph(sample(range(7), error_number))
            graph.simplify()
            self.assertFalse(HammingTest.is_identity(graph))
