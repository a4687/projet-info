import unittest
from modules.node import Node


class NodeTest(unittest.TestCase):
    def setUp(self):
        self.n0 = Node('a', 1, {}, {})

    def test_get_id(self):
        self.assertEqual(self.n0.id, 1)

    def test_get_label(self):
        self.assertEqual(self.n0.label, 'a')

    def test_copy(self):
        self.assertIsNot(self.n0.copy(), self.n0)
