from random import randint
import unittest
from modules.bool_circ import BoolCirc


class AdderTest(unittest.TestCase):
    TEST_NUMBER = 5

    def test_half_adder(self):
        for _ in range(self.TEST_NUMBER):
            a = randint(0, 255)
            b = randint(0, 255)

            circ = BoolCirc.half_adder(3)  # we work on 2^3 = 8 bit registers
            circ.set_input_value('a', a)
            circ.set_input_value('b', b)
            circ.set_input_value('c', 1)
            circ.evaluate()

            self.assertEqual(circ.get_output('r'), (a + b + 1) % 256)
            self.assertEqual(circ.get_output("c'"), (a + b + 1) // 256)

    def test_adder(self):
        for _ in range(self.TEST_NUMBER):
            a = randint(0, 255)
            b = randint(0, 255)

            circ = BoolCirc.adder(3)  # we work on 2^3 = 8 bit registers
            circ.set_input_value('a', a)
            circ.set_input_value('b', b)
            circ.evaluate()

            self.assertEqual(circ.get_output('r'), (a + b) % 256)
            self.assertEqual(circ.get_output("c'"), (a + b) // 256)

    def test_carry_lookahead_half_adder(self):
        for _ in range(self.TEST_NUMBER):
            a = randint(0, 255)
            b = randint(0, 255)

            circ = BoolCirc.carry_lookahead_half_adder(2)  # we work on 2*4 = 8 bit registers
            circ.set_input_value('a', a)
            circ.set_input_value('b', b)
            circ.set_input_value('c', 1)
            circ.evaluate()

            self.assertEqual(circ.get_output('r'), (a + b + 1) % 256)
            self.assertEqual(circ.get_output("c'"), (a + b + 1) // 256)

    def test_carry_lookahead_adder(self):
        for _ in range(self.TEST_NUMBER):
            a = randint(0, 255)
            b = randint(0, 255)

            circ = BoolCirc.carry_lookahead_adder(2)  # we work on 2*4 = 8 bit registers
            circ.set_input_value('a', a)
            circ.set_input_value('b', b)
            circ.evaluate()

            self.assertEqual(circ.get_output('r'), (a + b) % 256)
            self.assertEqual(circ.get_output("c'"), (a + b) // 256)
