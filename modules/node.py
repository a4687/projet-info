from __future__ import annotations
from typing import NewType, Dict, List

Id = NewType('Id', int)


class Node:
    """ Node Class, representative of Vertices in a Graph. """

    def __init__(self, label: str, id: Id, parents: Dict[Id, int], children: Dict[Id, int]):
        """
        :param label:
        :param id: unique id in the graph
        :param parents: maps a parent node's id to its multiplicity
        :param children: maps a child node's id to its multiplicity """

        self._label = label
        self._id = id
        self._parents = parents
        self._children = children

    # region Elementary setters and getters
    @property
    def id(self) -> Id:
        """ Node unique ID.

        :return: node ID """
        return self._id

    @id.setter
    def id(self, new_id: Id):
        """ Set new node ID.

        :param new_id: new node ID """
        self._id = new_id

    @property
    def label(self) -> str:
        """ Node Label.

        :return: label """
        return self._label

    @label.setter
    def label(self, new_label: str):
        """ Set Node Label.

        :param new_label: New Node Label """
        self._label = new_label

    @property
    def parents_ids(self) -> Dict[Id, int]:
        """ Dictionary which maps Parents Id to multiplicity.

        :return: Dictionary (Id -> int) """
        return self._parents

    @parents_ids.setter
    def parents_ids(self, new_parents_ids: Dict[Id, int]):
        """ Set Dictionary which maps Parents Id to multiplicity.

        :param new_parents_ids: New Parents Dictionary """
        self._parents = new_parents_ids

    @property
    def children_ids(self) -> Dict[Id, int]:
        """ Dictionary which maps Children Id to multiplicity.

        :return: Dictionary (Id -> int) """
        return self._children

    @children_ids.setter
    def children_ids(self, new_children_ids: Dict[Id, int]):
        """ Set Dictionary which maps Children Id to multiplicity.

        :param new_children_ids: New Children Dictionary """
        self._children = new_children_ids

    @property
    def parents(self) -> List[Id]:
        """ Return Id of all Parents Node.

        :return: Parent Id List """
        return list(self.parents_ids)

    @property
    def children(self) -> List[Id]:
        """ Return Id of all Children Node.

        :return: Children Id List """
        return list(self.children_ids)
    # endregion

    # region Degrees
    @property
    def degree_in(self) -> int:
        """ Return number of nodes before
        :return: number of parents
        """
        return sum([mult for mult in self.parents_ids.values()])

    @property
    def degree_out(self) -> int:
        """ Return number of nodes after
        :return: number of children
        """
        return sum([mult for mult in self.children_ids.values()])

    @property
    def degree(self) -> int:
        """ Return number of nodes before and after
        :return: number of parents and children
        """
        return self.degree_in + self.degree_out
    # endregion

    def __str__(self) -> str:
        return f"Node '{self._label}' (Id: {self._id}, Parents: {self._parents}," \
               f" Children: {self._children}) "

    def __repr__(self) -> str:
        return str(self)

    def copy(self) -> Node:
        """ Build a new independent Node.

        :return: New Node """
        return Node(self._label, self._id, self._parents.copy(), self._children.copy())

    # region Children Management
    def add_child_id(self, child_id: Id):
        """ Add a multiplicity to specified Id.

        :param child_id: Node Id to witch add a multiplicity """
        if child_id in self.children:
            self.children_ids[child_id] += 1
        else:
            self.children_ids[child_id] = 1

    def remove_child_once(self, child_id: Id):
        """ Remove a multiplicity to specified Id.

        :param child_id: Node Id to witch remove a multiplicity """
        if child_id in self._children.keys():
            self._children[child_id] -= 1
            if self._children[child_id] <= 0:
                self._children.pop(child_id)

    def remove_child_id(self, child_id: Id):
        """ Remove all multiplicity to specified Id.

        :param child_id: Node Id to witch remove all multiplicity """
        if child_id in self.children:
            self.children_ids.pop(child_id)
    # endregion

    # region Parents management
    def add_parent_id(self, parent_id: Id):
        """ Add a multiplicity to specified Id.

        :param parent_id: Node Id to witch add a multiplicity """
        if parent_id in self.parents:
            self.parents_ids[parent_id] += 1
        else:
            self.parents_ids[parent_id] = 1

    def remove_parent_once(self, parent_id: Id):
        """ Remove a multiplicity to specified Id.

        :param parent_id: Node Id to witch remove a multiplicity """
        if parent_id in self.parents:
            self.parents_ids[parent_id] -= 1
            if self.parents_ids[parent_id] <= 0:
                self.parents_ids.pop(parent_id)

    def remove_parent_id(self, parent_id: Id):
        """ Remove all multiplicity to specified Id.

        :param parent_id: Node Id to witch remove all multiplicity """
        if parent_id in self.parents:
            self.parents_ids.pop(parent_id)
    # endregion

