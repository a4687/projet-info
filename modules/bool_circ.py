from __future__ import annotations

import math
import re
from enum import Enum
from random import choice, randint
from typing import List, Tuple, Union

from modules.node import Id, Node
from modules.open_digraph import OpenDigraph, DigraphType
from modules.parser import Parser


class BoolNodeTypes(str, Enum):
    # Useful function
    COPY = ""
    NOT = "~"

    # Operation
    AND = "&"
    OR = "|"
    XOR = "^"

    # Values
    TRUE = "1"
    FALSE = "0"


class BoolCirc(OpenDigraph):
    """ BoolCirc class, Bool circuit Open Digraph """

    def __init__(self, digraph: OpenDigraph):
        self._node_regex = re.compile("^([a-z]+'*)(\d*)$")

        if isinstance(digraph, OpenDigraph):
            d = digraph.copy()
            super(BoolCirc, self).__init__(d.inputs_ids, d.outputs_ids, d.nodes)
            self._name_output_nodes()

            for i, input_id in enumerate(self.inputs_ids):
                self.get_node_by_id(input_id).label = f"x{i}"

            if not self.is_well_formed():
                raise ValueError("Given 'OpenDigraph' does not meet 'BoolCirc' requirement.")

            self._p = Parser()
            self._p.add_operation(BoolNodeTypes.NOT, True)
            self._p.add_operation(BoolNodeTypes.AND)
            self._p.add_operation(BoolNodeTypes.OR)
            self._p.add_operation(BoolNodeTypes.XOR)
        else:
            raise ValueError("You must pass a 'OpenDigraph' object to 'BoolCirc' constructor")

    def decompose_node_name(self, node_id: Id) -> Tuple[str, int]:
        """ give the var and the number of the node
        each inputs or outputs is named by a letter and a number
        :param node_id:
        :return: var and number of the node
        """
        # only output and inputs can be named
        if node_id not in self.inputs_ids + self.outputs_ids:
            raise ValueError("Specified node can't be named.")

        node_label = self.get_node_by_id(node_id).label

        match = self._node_regex.match(node_label)
        var = match.group(1)
        var_pos = int(match.group(2)) if match.group(2) else 0
        return var, var_pos

    def is_well_formed(self) -> bool:
        """ check if self is well-formed
        :return: self is well-formed
        """
        if not OpenDigraph.is_well_formed(self):
            return False
        elif self.is_cyclic():
            return False
        else:
            for node in self.middle_nodes:  # We check all node unless the inputs and outputs
                if (node.label == BoolNodeTypes.AND
                    or node.label == BoolNodeTypes.OR
                    or node.label == BoolNodeTypes.XOR) \
                        and node.degree_out != 1:
                    # AND, OR, XOR node have only one exit
                    return False
                elif node.label == BoolNodeTypes.NOT and \
                        (node.degree_out != 1
                         or node.degree_in != 1):
                    # NOT node have only one entry and one exit
                    return False
                elif node.label == BoolNodeTypes.COPY and node.degree_in != 1:
                    # COPY node have only one entry
                    return False
                elif (node.label == BoolNodeTypes.TRUE or node.label == BoolNodeTypes.FALSE) \
                        and (node.degree_in != 0 and node.degree_out != 1):
                    # TRUE and FALSE must have only one exit and no entry
                    return False
                elif node.label not in [BoolNodeTypes.AND, BoolNodeTypes.OR, BoolNodeTypes.XOR,
                                        BoolNodeTypes.NOT, BoolNodeTypes.COPY, BoolNodeTypes.TRUE,
                                        BoolNodeTypes.FALSE]:
                    # label must be a BoolNodeType
                    return False

            for input_id in self.inputs_ids:
                input_node = self.get_node_by_id(input_id)
                if self._node_regex.match(input_node.label) is None:
                    return False

            for output_id in self.outputs_ids:
                output_node = self.get_node_by_id(output_id)
                if self._node_regex.match(output_node.label) is None:
                    return False

        return True

    @classmethod
    def empty(cls) -> BoolCirc:
        """ Create an empty BoolCirc.

        :return: Empty BoolCirc """
        return cls(OpenDigraph.empty())

    @classmethod
    def parse_parentheses(cls, *args: List[str]) -> BoolCirc:
        """ return a bool circ representing the logical expressions inserted
        :param args: logical expression
        :return: BoolCirc representing args
        """
        g = cls.empty()

        for string in args:
            current_node_id = g.add_node("")
            g.add_output_node(current_node_id, "")

            name_buffer = ""

            for char in string:
                if char == '(':
                    if g.get_node_by_id(current_node_id).label != name_buffer:
                        g.get_node_by_id(current_node_id).label += name_buffer
                    parent_node_id = g.add_node("")
                    g.add_edge(parent_node_id, current_node_id)
                    current_node_id = parent_node_id
                    name_buffer = ""
                elif char == ")":
                    g.get_node_by_id(current_node_id).label += name_buffer
                    current_node_id = g.get_node_by_id(current_node_id).children[0]
                    name_buffer = ""
                else:
                    name_buffer += char

        g._merge_input_nodes()
        g._name_output_nodes()
        return g

    @classmethod
    def parse(cls, *args: List[str]) -> BoolCirc:
        """ Do the same as parse_parentheses but without the useless parentheses inside the str list
        return a bool circ representing the logical expressions inserted
        :param args: logical expression
        :return: BoolCirc representing args
        """
        g = cls.empty()

        def loop(node_id: Id, parsed_obj: List[str]):
            node = g.get_node_by_id(node_id)

            if isinstance(parsed_obj, list):
                node.label = parsed_obj[0]
                for parent in parsed_obj[1:]:
                    new_id = g.add_node()
                    g.add_edge(new_id, node_id)
                    loop(new_id, parent)
            else:  # parsed_obj is a string
                node.label = parsed_obj

        for string in args:
            current_node_id = g.add_node("")
            g.add_output_node(current_node_id, "")
            parsed_list = g._p.parse(string)
            loop(current_node_id, parsed_list)

        g._merge_input_nodes()
        g._name_output_nodes()

        return g

    def _merge_input_nodes(self):
        """ merge the input having the same nam in one
        """
        leaf_list = sorted(self.leaf_list(False), key=lambda node: node.label)  # Sorted by name

        if leaf_list:
            current_leaf = leaf_list[0]

            for leaf in leaf_list[1:]:
                if current_leaf.label == leaf.label:
                    self.merge(current_leaf.id, leaf.id)
                else:
                    self.add_input_node(current_leaf.id, current_leaf.label)
                    current_leaf.label = BoolNodeTypes.COPY
                    current_leaf = leaf

            self.add_input_node(current_leaf.id, current_leaf.label)
            current_leaf.label = BoolNodeTypes.COPY

    def _name_output_nodes(self):
        """ give a name to each output : yk, k in Naturals
        """
        for i, output_id in enumerate(self.outputs_ids):
            self.get_node_by_id(output_id).label = f"y{i}"

    # region build a graph with a truth table (TD9: Grey Code, Karnaugh ...)
    @staticmethod
    def _extract_power_of_2(n: int) -> int:
        """ if n is a power of two, give its log2
        :param n: int
        :return: log2(n)
        """
        log2_n = math.log2(n)
        nb_var = int(log2_n)
        if log2_n != nb_var:
            raise ValueError(f"{n} is not a power of 2")
        else:
            return nb_var

    @staticmethod
    def to_bin(num: int, nb_bits: int = None) -> str:
        """ give num in binary base
        :param num: the num converted in binary
        :param nb_bits: the nb of bit we want or num to be expressed with
        :return: num in binary (string)
        """
        if nb_bits is None:
            return bin(num)[2:]
        else:
            # if num > 2^nb_bits, return num mod 2^nb_bits to fit the bit size
            return ("{:0" + str(nb_bits) + "b}").format(num)[:nb_bits]

    @classmethod
    def table_to_graph(cls, bit_list: str) -> BoolCirc:
        """ Convert a string of bits (ie. '1110001000111111') to a bool circuit

        Depth = 4 (Copy -> (Not ?) -> And -> Final Or)
        Gates ~ p*AND + 1*OR + 1/2*p*n*NOT + n*COPY = O(p*n)
        p : Hamming weight of bit_list, n : number of vars

        :param bit_list: string of bits
        :return: BoolCirc """
        g = cls.empty()
        nb_var = g._extract_power_of_2(len(bit_list))

        list_id_var = []
        id_or = g.add_node("|")  # Final OR node
        g.add_output_node(id_or, 'y0')

        for i in range(nb_var):
            new_copy_id = g.add_node(BoolNodeTypes.COPY)
            g.add_input_node(new_copy_id, f"x{i}")
            list_id_var += [new_copy_id]

        for bit_num, bit in enumerate(bit_list):
            if bit == "1":
                id_and = g.add_node("&")
                g.add_edge(id_and, id_or)
                bin_line = BoolCirc.to_bin(bit_num, nb_var)
                for var_num, bin_char in enumerate(bin_line):
                    if bin_char == '0':
                        id_not = g.add_node("~")
                        g.add_edge(list_id_var[var_num], id_not)
                        g.add_edge(id_not, id_and)
                    else:
                        g.add_edge(list_id_var[var_num], id_and)

        return g

    @staticmethod
    def _next_gray_list(gray_list: List[str]) -> List[str]:
        """ give the next rank grey
        :param gray_list: previous grey list
        :return: next rank grey list
        """
        list_with_0 = []
        list_with_1 = []

        for gray_code_elm in gray_list:
            # add 0 in front of every string of gray_list
            list_with_0 += ["0" + gray_code_elm]
            # add 1 in front of reversed gray_list
            list_with_1 = ["1" + gray_code_elm] + list_with_1

        return list_with_0 + list_with_1

    @staticmethod
    def gray_code(bit_nb: int) -> List[str]:
        """ Return the list of the gray code of 'bit_nb' bit size

        :param bit_nb: bits number
        :return: code list """
        if bit_nb < 1:
            raise ValueError("bit_num must be greater than 1")
        elif bit_nb == 1:
            return ["0", "1"]
        else:
            return BoolCirc._next_gray_list(BoolCirc.gray_code(bit_nb - 1))

    @staticmethod
    def karnaugh_map(bit_list: str) -> List[List[int]]:
        """ Return the corresponding Karnaugh table

        :param bit_list: string of bits
        :return: Table of bits """
        nb_bits = len(bit_list)
        nb_var = BoolCirc._extract_power_of_2(nb_bits)
        half_nb_var = nb_var // 2

        gray_for_y = BoolCirc.gray_code(half_nb_var)
        nb_y_vars = len(gray_for_y)

        if nb_var % 2 == 0:
            gray_for_x = gray_for_y
            nb_x_vars = nb_y_vars
        else:
            gray_for_x = BoolCirc._next_gray_list(gray_for_y)
            nb_x_vars = 2 * nb_y_vars

        table = [[int(bit_list[int(gray_for_y[y] + gray_for_x[x], 2)])
                  for x in range(nb_x_vars)]
                 for y in range(nb_y_vars)]

        return table

    @staticmethod
    def _count_one(table: List[List[int]], x: int, y: int, width: int, height: int) \
            -> Tuple[bool, int]:
        """ Count the number of 1 (and -1) in the rectangle (x, y, width, height).

        :param table: Table to use
        :param x: Left Border of the rectangle
        :param y: Top Border of the rectangle
        :param width: Width of the rectangle
        :param height: Height of the rectangle
        :return: True with the number of abs(one) if the whole rectangle is composed of 1 (or -1),
         false and 0 otherwise
        """
        table_height = len(table[0])
        table_width = len(table)

        cpt = 0

        for w in range(width):
            for h in range(height):
                cell = table[(y + h) % table_height][(x + w) % table_width]
                if abs(cell) != 1:
                    return False, 0
                elif cell == 1:
                    cpt += 1

        return True, cpt

    @staticmethod
    def _set_to_minus_one(table: List[List[int]], x: int, y: int, width: int, height: int):
        """ Mark the rectangle as used. The one became -one.

        :param table: Table to use
        :param x: Left Border of the rectangle
        :param y: Top Border of the rectangle
        :param width: Width of the rectangle
        :param height: Height of the rectangle
        :return: Nothing :)
        """
        table_height = len(table[0])
        table_width = len(table)

        for w in range(width):
            for h in range(height):
                table[(y + h) % table_height][(x + w) % table_width] = -1

    @staticmethod
    def _find_best_block(table: List[List[int]], width: int, height: int) \
            -> Union[Tuple[int], None]:
        """ Find the bloc with all abs(one) with the maximum of one.

        :param table: Table used
        :param width: max width of the bloc
        :param height: max height of the bloc
        :return: None if no bloc found, (x, y, width, height) otherwise
        """
        table_height = len(table[0])
        table_width = len(table)

        max_one_number = 0
        max_bloc = ()

        if table_height == height:
            if table_width == width:  # width == table_width && table_height == height
                if BoolCirc._count_one(table, 0, 0, width, height)[0]:
                    max_bloc = (0, 0, width, height)
            else:  # width < table_width && height == table_height
                for x_possible in range(table_width):
                    is_ok, one_number = BoolCirc._count_one(table, x_possible, 0, width, height)
                    if is_ok and max_one_number < one_number:
                        max_one_number = one_number
                        max_bloc = (x_possible, 0, width, height)
        else:
            if table_width == width:  # width == table_width && height < table_height
                for y_possible in range(table_height):
                    is_ok, one_number = BoolCirc._count_one(table, 0, y_possible, width, height)
                    if is_ok and max_one_number < one_number:
                        max_one_number = one_number
                        max_bloc = (0, y_possible, width, height)
            else:  # width < table_width && height < table_height
                for x_possible in range(table_width):
                    for y_possible in range(table_height):
                        is_ok, one_number = BoolCirc._count_one(table, x_possible, y_possible,
                                                                width,
                                                                height)
                        if is_ok and max_one_number < one_number:
                            max_one_number = one_number
                            max_bloc = (x_possible, y_possible, width, height)

        if max_bloc == ():
            return None
        else:
            BoolCirc._set_to_minus_one(table, max_bloc[0], max_bloc[1], max_bloc[2], max_bloc[3])
            return max_bloc

    @staticmethod
    def _tile_with_blocks(table: List[List[int]]) -> List[Tuple[int]]:
        """ Tile the table with the biggest blocs of one possible.
        Width and height of any bloc are a power of two.
        
        :param table: table used
        :return: List of Blocs described as  (x, y, width, height)
        """
        nb_var_x = BoolCirc._extract_power_of_2(len(table))
        nb_var_y = BoolCirc._extract_power_of_2(len(table[0]))
        nb_var = nb_var_x + nb_var_y

        list_of_blocks = []

        for k in range(nb_var, -1, -1):

            # On va chercher des blocs de taille 2**k
            for i in range(max(k - nb_var_y, 0), min(nb_var_x, k) + 1):
                width = 2 ** i  # 0 <= i <= nb_var_x
                height = 2 ** (k - i)  # 0 <= k - i <= nb_var_y
                rep = BoolCirc._find_best_block(table, width, height)
                if rep is not None:
                    list_of_blocks += [rep]

        return list_of_blocks

    @staticmethod
    def _filter_block_tile(block_list: List[Tuple[int]], table_width: int, table_height: int) \
            -> List[Tuple[int]]:
        """ Remove all the smallest bloc that are already covered by the tile found.
        
        :param block_list: Tile used (List of blocs to analyse)
        :param table_width: Width of the Table
        :param table_height: Height of the Table
        :return: Filtered Tile of the Table
        """

        def in_block(x: int, y: int, block: Tuple[int]) -> bool:
            """ Test if (x, y) is in block

            :param x: abscissa
            :param y: ordinate
            :param block: block
            :return: boolean """

            min_x, max_x = block[0], block[0] + block[2] - 1
            min_y, max_y = block[1], block[1] + block[3] - 1

            if max_x == max_x % table_width:  # No 'looping' block for x coord
                x_ok = min_x <= x <= max_x
            else:
                x_ok = x <= max_x % table_width or x >= min_x

            if max_y == max_y % table_height:  # No 'looping' block for y coord
                y_ok = min_y <= y <= max_y
            else:
                y_ok = y <= max_y % table_height or y >= min_y

            return x_ok and y_ok

        def in_union(block_c: Tuple[int], block_1: Tuple[int], block_2: Tuple[int]) -> bool:
            """ Test if block_c is in (block_1 U block_2)

            :param block_c: block
            :param block_1: block
            :param block_2: block
            :return: bool """

            min_x, max_x = block_c[0], (block_c[0] + block_c[2] - 1) % table_width
            min_y, max_y = block_c[1], (block_c[1] + block_c[3] - 1) % table_height

            return (in_block(min_x, min_y, block_1) or in_block(min_x, min_y, block_2)) and \
                   (in_block(min_x, max_y, block_1) or in_block(min_x, max_y, block_2)) and \
                   (in_block(max_x, min_y, block_1) or in_block(max_x, min_y, block_2)) and \
                   (in_block(max_x, max_y, block_1) or in_block(max_x, max_y, block_2))

        def pop_if_composed_block(current_block_id: int, current_block: Tuple[int]):
            for i in range(len(block_list)):
                block_i = block_list[i]
                for j in range(len(block_list)):
                    block_j = block_list[j]
                    if i != current_block_id and j != current_block_id and i != j \
                            and in_union(current_block, block_i, block_j):
                        block_list.pop(current_block_id)
                        return

        k = len(block_list) - 1

        while k > 0:
            pop_if_composed_block(k, block_list[k])
            k -= 1

        return block_list

    @staticmethod
    def _karnaugh_to_expression(table: List[List[int]]) -> str:
        """ Convert Karnaugh table to boolean expression

        :param table: karnaugh table
        :return: expression """
        list_of_blocks = BoolCirc._tile_with_blocks(table)
        table_width = len(table)
        table_height = len(table[0])

        list_of_blocks = BoolCirc._filter_block_tile(list_of_blocks, table_height, table_width)

        nb_var_x = BoolCirc._extract_power_of_2(table_width)
        nb_var_y = BoolCirc._extract_power_of_2(table_height)

        gray_for_y = BoolCirc.gray_code(nb_var_y)

        if nb_var_x == nb_var_y:
            gray_for_x = gray_for_y
        else:
            gray_for_x = BoolCirc._next_gray_list(gray_for_y)

        def and_bit_list(bit_list: List[str]) -> str:
            """ give the And of bit_List
            :param bit_list: list of bits (string)
            :return: bit_List[0] & bit_List[1] & ...
            """
            bit_length = len(bit_list[0])
            value = int("1" * bit_length, 2)

            for bit_str in bit_list:
                if len(bit_str) != bit_length:
                    raise ValueError("All bit_str must have the same size.")
                value = value & int(bit_str, 2)

            return BoolCirc.to_bin(value, bit_length)

        def not_and_bit_list(bit_list: List[str]) -> str:
            """ give the Nand of a list of bit

            :param bit_list: list of bits
            :return: nand of bit list
            """

            def bit_not(a: str) -> int:
                return int("1" * len(a), 2) - int(bit_str, 2)

            bit_length = len(bit_list[0])
            value = int("1" * bit_length, 2)

            for bit_str in bit_list:
                if len(bit_str) != bit_length:
                    raise ValueError("All bit_str must have the same size.")
                value = value & bit_not(bit_str)

            return BoolCirc.to_bin(value, bit_length)

        def extract_invariants(bit_string: str, prefix: str = "") -> List[str]:
            invar_list = []
            for var_num, var_type in enumerate(bit_string):
                if var_type == '1':  # if var_num is invariant
                    invar_list += [f"{prefix}x{var_num}"]  # it goes to the list !
            return invar_list

        def block_to_expr(current_block: Tuple[int, int, int, int]) -> str:
            block_x, block_y, block_width, block_height = current_block

            gray_block_x = [gray_for_x[(block_x + i) % table_width] for i in range(block_width)]
            gray_block_y = [gray_for_y[(block_y + i) % table_height] for i in
                            range(block_height)]

            gray_block_comb = [elm_y + elm_x for elm_x in gray_block_x for elm_y in
                               gray_block_y]

            # invariants for 1 are put to 1, every other to 0
            one_invariants = extract_invariants(and_bit_list(gray_block_comb))

            # invariants for 0 are put to 1, every other to 0
            zero_invariants = extract_invariants(not_and_bit_list(gray_block_comb), prefix="~")

            # We write the expression of the block
            return " & ".join(one_invariants + zero_invariants)

        block_expr = [f"({block_to_expr(block)})" for block in list_of_blocks]
        expression = " | ".join(block_expr)

        return expression

    @classmethod
    def table_to_graph2(cls, bit_list: str):
        """ Convert a string of bits (ie. '1110001000111111') to a bool circuit

        :param bit_list: string of bits
        :return: BoolCirc """
        return cls.parse(BoolCirc._karnaugh_to_expression(BoolCirc.karnaugh_map(bit_list)))

    # endregion

    @classmethod
    def random_bool_circ(cls, node_nb: int, input_nb: int = None, output_nb: int = None):
        """ Create Random BoolCirc

        :param node_nb: Number of Node
        :param input_nb: Number of inputs
        :param output_nb: Number of outputs
        :return: New Random BoolCirc """

        # Create a random oriented graph with the right number of inputs and outputs
        d = OpenDigraph.random(node_nb, 2, DigraphType.Acyclic, 0, 0)

        for leaf in d.leaf_list(False):
            d.add_input_node(leaf.id)

        for leaf in d.leaf_list():
            d.add_output_node(leaf.id)

        if input_nb is not None and input_nb >= 1:
            while input_nb > len(d.inputs_ids):
                d.add_input_node(choice(d.middle_nodes).id)
            while input_nb < len(d.inputs_ids):
                input1_id = d.inputs_ids.pop(randint(0, len(d.inputs_ids) - 1))
                input2_id = d.inputs_ids.pop(randint(0, len(d.inputs_ids) - 1))
                d.merge(input1_id, input2_id)
                d.add_input_node(input1_id)

        if output_nb is not None and output_nb >= 1:
            while output_nb > len(d.outputs_ids):
                d.add_output_node(choice(d.middle_nodes).id)
            while output_nb < len(d.outputs_ids):
                output1_id = d.outputs_ids.pop(randint(0, len(d.outputs_ids) - 1))
                output2_id = d.outputs_ids.pop(randint(0, len(d.outputs_ids) - 1))
                d.merge(output1_id, output2_id)
                d.add_output_node(output1_id)

        for node in d.middle_nodes:
            if node.degree_in == 1 and node.degree_out == 1:
                node.label = BoolNodeTypes.NOT
            elif node.degree_in == 1 and node.degree_out > 1:
                node.label = BoolNodeTypes.COPY
            elif node.degree_in > 1 and node.degree_out == 1:
                node.label = choice([BoolNodeTypes.AND, BoolNodeTypes.OR, BoolNodeTypes.XOR])
            elif node.degree_in > 1 and node.degree_out > 1:
                new_label = choice([BoolNodeTypes.AND, BoolNodeTypes.OR, BoolNodeTypes.XOR])
                new_parent = d.add_node(new_label)

                for parent_id, parent_multiplicity in list(node.parents_ids.items()):
                    d.remove_parallel_edge(parent_id, node.id)
                    for _ in range(parent_multiplicity):
                        d.add_edge(parent_id, new_parent)

                d.add_edge(new_parent, node.id)
                node.label = BoolNodeTypes.COPY
            else:
                raise RuntimeError()

        return cls(d)

    # region Adders
    def _uncarryfy(self):
        """ Remove the input carry of an adder by setting it to 0.

        :return: None
        """
        for input_id in self.inputs_ids:
            if self.get_node_by_id(input_id).label == 'c':
                child_id = self.get_node_by_id(input_id).children[0]
                self.remove_node_by_id(input_id)
                carry_0 = self.add_node(BoolNodeTypes.FALSE)
                self.add_edge(carry_0, child_id)
                return

        raise RuntimeError("Why can't I find the carry ?")

    @classmethod
    def _build_half_adder(cls, n: int) -> Tuple[BoolCirc, List[Id], List[Id], List[Id], Id, Id]:
        """ Build (recursively) the Half-Adder of rank n (with 2^n register input size)

        :param n: rank of the adder
        :return: The Adder Boolean Circuit !
        """
        if n < 0:
            raise ValueError("Cannot have a n negative.")
        elif n == 0:
            d = BoolCirc.empty()
            final_or = d.add_node(BoolNodeTypes.OR)
            out_carry = d.add_output_node(final_or)

            left_and = d.add_node(BoolNodeTypes.AND)
            middle_and = d.add_node(BoolNodeTypes.AND)
            d.add_edge(left_and, final_or)
            d.add_edge(middle_and, final_or)

            right_xor = d.add_node(BoolNodeTypes.XOR)
            out_sum = d.add_output_node(right_xor)

            right_copy = d.add_node(BoolNodeTypes.COPY)
            d.add_edge(right_copy, right_xor)
            d.add_edge(right_copy, middle_and)
            in_carry = d.add_input_node(right_copy)

            middle_bottom_copy = d.add_node(BoolNodeTypes.COPY)

            d.add_edge(middle_bottom_copy, middle_and)
            d.add_edge(middle_bottom_copy, right_xor)

            middle_xor = d.add_node(BoolNodeTypes.XOR)
            d.add_edge(middle_xor, middle_bottom_copy)

            left_copy = d.add_node(BoolNodeTypes.COPY)
            d.add_edge(left_copy, left_and)
            d.add_edge(left_copy, middle_xor)

            middle_top_copy = d.add_node(BoolNodeTypes.COPY)
            d.add_edge(middle_top_copy, left_and)
            d.add_edge(middle_top_copy, middle_xor)

            in_a = d.add_input_node(left_copy)
            in_b = d.add_input_node(middle_top_copy)
            return d, [in_a], [in_b], [out_sum], in_carry, out_carry
        else:
            prev_adder1, in1_a, in1_b, out1_sum, in1_carry, out1_carry = cls._build_half_adder(
                n - 1)
            prev_adder2 = prev_adder1.copy()
            in2_a = in1_a.copy()
            in2_b = in1_b.copy()
            out2_sum = out1_sum.copy()
            in2_carry = in1_carry
            out2_carry = out1_carry

            shift_val = prev_adder1.parallel_shift(prev_adder2)
            prev_adder1.iparallel(prev_adder2)
            in1_a = [node_id + shift_val for node_id in in1_a]
            in1_b = [node_id + shift_val for node_id in in1_b]
            out1_sum = [node_id + shift_val for node_id in out1_sum]
            in1_carry += shift_val
            out1_carry += shift_val

            child_input = prev_adder1.get_node_by_id(in2_carry).children[0]
            parent_output = prev_adder1.get_node_by_id(out1_carry).parents[0]

            prev_adder1.remove_node_by_id(in2_carry)
            prev_adder1.remove_node_by_id(out1_carry)

            prev_adder1.add_edge(parent_output, child_input)

            return (prev_adder1, in1_a + in2_a, in1_b + in2_b, out1_sum + out2_sum, in1_carry,
                    out2_carry)

    @classmethod
    def half_adder(cls, n: int) -> BoolCirc:
        """ Return the Half-Adder circuit with register of size 2^n.
        Inputs values are a, b and c (in carry). Output Values are r (the result) and c' (out carry)

        :param n: rank of the adder
        :return: The Beautiful Circuit
        """
        half_adder, in_a, in_b, out_sum, in_carry, out_carry = cls._build_half_adder(n)

        for i, node_id in enumerate(in_a):
            node = half_adder.get_node_by_id(node_id)
            node.label = f"a{i}"

        for i, node_id in enumerate(in_b):
            node = half_adder.get_node_by_id(node_id)
            node.label = f"b{i}"

        for i, node_id in enumerate(out_sum):
            node = half_adder.get_node_by_id(node_id)
            node.label = f"r{i}"

        half_adder.get_node_by_id(in_carry).label = 'c'
        half_adder.get_node_by_id(out_carry).label = "c'"

        return half_adder

    @classmethod
    def adder(cls, n: int) -> BoolCirc:
        """ Return the Adder circuit with register of size 2^n.
        Inputs values are a, b. Output Values are r (the result) and c' (out carry)

        :param n: rank of the adder
        :return: The Beautiful Recursively Made Circuit
        """
        adder = cls.half_adder(n)
        adder._uncarryfy()
        return adder

    @classmethod
    def _build_carry_lookahead(cls, n: int) -> Tuple[BoolCirc, List[Id], List[Id], List[Id], Id]:
        """ Return a recursive built Carry-Lookahead with register size of 4n.

        Circuit Depth ~ 3n = O(n)
        Gates Number ~ 22n = O(n)

        :param n: rank
        :return: The Carry-Lookahead """
        if n <= 0:
            raise ValueError("Cannot have a n negative or null.")
        elif n == 1:
            carry_lh = cls.empty()
            copy_carry = carry_lh.add_node(BoolNodeTypes.COPY)
            copy_generate = []
            copy_propagate = []

            for i in range(4):
                copy_propagate += [carry_lh.add_node(BoolNodeTypes.COPY)]
                copy_generate += [carry_lh.add_node(BoolNodeTypes.COPY)]

            # g0 ^ (p0 & c0)
            and1_1 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[0], and1_1)
            carry_lh.add_edge(copy_carry, and1_1)

            xor1 = carry_lh.add_node(BoolNodeTypes.XOR)
            carry_lh.add_edge(copy_generate[0], xor1)
            carry_lh.add_edge(and1_1, xor1)

            # "g1 ^ (p1 & g0) ^ (p1 & p0 & c0)
            and2_1 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[1], and2_1)
            carry_lh.add_edge(copy_generate[0], and2_1)

            and2_2 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[1], and2_2)
            carry_lh.add_edge(copy_propagate[0], and2_2)
            carry_lh.add_edge(copy_carry, and2_2)

            xor2 = carry_lh.add_node(BoolNodeTypes.XOR)
            carry_lh.add_edge(copy_generate[1], xor2)
            carry_lh.add_edge(and2_1, xor2)
            carry_lh.add_edge(and2_2, xor2)

            # g2 ^ (p2 & g1) ^ (p2 & p1 & g0) ^ (p2 & p1 & p0 & c0)
            and3_1 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[2], and3_1)
            carry_lh.add_edge(copy_generate[1], and3_1)

            and3_2 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[2], and3_2)
            carry_lh.add_edge(copy_propagate[1], and3_2)
            carry_lh.add_edge(copy_generate[0], and3_2)

            and3_3 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[2], and3_3)
            carry_lh.add_edge(copy_propagate[1], and3_3)
            carry_lh.add_edge(copy_propagate[0], and3_3)
            carry_lh.add_edge(copy_carry, and3_3)

            xor3 = carry_lh.add_node(BoolNodeTypes.XOR)
            carry_lh.add_edge(copy_generate[2], xor3)
            carry_lh.add_edge(and3_1, xor3)
            carry_lh.add_edge(and3_2, xor3)
            carry_lh.add_edge(and3_3, xor3)

            # g3 ^ (p3 & g2) ^ (p3 & p2 & g1) ^ (p3 & p2 & p1 & g0) ^ (p3 & p2 & p1 & p0 & c0)
            and4_1 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[3], and4_1)
            carry_lh.add_edge(copy_generate[2], and4_1)

            and4_2 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[3], and4_2)
            carry_lh.add_edge(copy_propagate[2], and4_2)
            carry_lh.add_edge(copy_generate[1], and4_2)

            and4_3 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[3], and4_3)
            carry_lh.add_edge(copy_propagate[2], and4_3)
            carry_lh.add_edge(copy_propagate[1], and4_3)
            carry_lh.add_edge(copy_generate[0], and4_3)

            and4_4 = carry_lh.add_node(BoolNodeTypes.AND)
            carry_lh.add_edge(copy_propagate[3], and4_4)
            carry_lh.add_edge(copy_propagate[2], and4_4)
            carry_lh.add_edge(copy_propagate[1], and4_4)
            carry_lh.add_edge(copy_propagate[0], and4_4)
            carry_lh.add_edge(copy_carry, and4_4)

            xor4 = carry_lh.add_node(BoolNodeTypes.XOR)
            carry_lh.add_edge(copy_generate[3], xor4)
            carry_lh.add_edge(and4_1, xor4)
            carry_lh.add_edge(and4_2, xor4)
            carry_lh.add_edge(and4_3, xor4)
            carry_lh.add_edge(and4_4, xor4)

            in_generate = []
            in_propagate = []

            for i in range(4):
                copy_generate_node = carry_lh.get_node_by_id(copy_generate[i])
                if copy_generate_node.degree_out == 1:
                    copy_generate_node.label = f"g{i}"
                    carry_lh.add_input_id(copy_generate_node.id)
                    in_generate += [copy_generate_node.id]
                else:
                    in_generate += [carry_lh.add_input_node(copy_generate_node.id, f"g{i}")]

                copy_propagate_node = carry_lh.get_node_by_id(copy_propagate[i])
                if copy_propagate_node.degree_out == 1:
                    copy_propagate_node.label = f"p{i}"
                    carry_lh.add_input_id(copy_propagate_node.id)
                    in_propagate += [copy_propagate_node.id]
                else:
                    in_propagate += [carry_lh.add_input_node(copy_propagate_node.id, f"p{i}")]

            out_carrys = [carry_lh.add_output_node(copy_carry),
                          carry_lh.add_output_node(xor1),
                          carry_lh.add_output_node(xor2),
                          carry_lh.add_output_node(xor3),
                          carry_lh.add_output_node(xor4)]

            in_carry = carry_lh.add_input_node(copy_carry, "c")

            return carry_lh, in_generate, in_propagate, out_carrys, in_carry
        else:
            carry_lh, in_gen, in_pro, out_car, in_car = cls._build_carry_lookahead(n - 1)
            carry_lh0, in_gen0, in_pro0, out_car0, in_car0 = cls._build_carry_lookahead(1)

            shift_val = carry_lh0.parallel_shift(carry_lh)
            in_gen0 = [node_id + shift_val for node_id in in_gen0]
            in_pro0 = [node_id + shift_val for node_id in in_pro0]
            out_car0 = [node_id + shift_val for node_id in out_car0]
            in_car0 += shift_val

            carry_lh0.iparallel(carry_lh)

            child_input = carry_lh0.get_node_by_id(in_car0).children[0]
            parent_output = carry_lh0.get_node_by_id(out_car[-1]).parents[0]

            carry_lh0.remove_node_by_id(in_car0)
            carry_lh0.remove_node_by_id(out_car[-1])

            carry_lh0.add_edge(parent_output, child_input)

            new_carry = carry_lh0
            new_in_gen = in_gen + in_gen0
            new_in_pro = in_pro + in_pro0
            new_out_car = out_car[:-1] + out_car0
            new_in_car = in_car

            for i, node_id in enumerate(new_in_gen):
                new_carry.get_node_by_id(node_id).label = f"g{i}"

            for i, node_id in enumerate(new_in_pro):
                new_carry.get_node_by_id(node_id).label = f"p{i}"

            for i, node_id in enumerate(new_out_car):
                new_carry.get_node_by_id(node_id).label = f"c{i}"

            new_carry.get_node_by_id(new_in_car).label = "c0"
            return new_carry, new_in_gen, new_in_pro, new_out_car, new_in_car

    @classmethod
    def carry_lookahead(cls, n: int) -> BoolCirc:
        """ Return a Carry-Lookahead circuit with register of size 4n.
        Inputs values are p (propagate), g (generate) and c0. Output Values are c (carrys)

        :param n: rank of the Carry-Lookahead
        :return: The Beautiful Recursively Made Carry-Lookahead
        """
        return cls._build_carry_lookahead(n)[0]

    @classmethod
    def carry_lookahead_half_adder(cls, n: int) -> BoolCirc:
        """ Return the Half Adder circuit build with a Carry-Lookahead with register of size 4n.
        Inputs values are a, b and c. Output Values are r (the result) and c' (out carry)

        :param n: rank of the adder
        :return: The Beautiful Recursively Made Circuit
        """
        cla = cls.empty()
        copy_a_n = []
        copy_b_n = []

        for i in range(4 * n):
            copy_a_n += [cla.add_node(BoolNodeTypes.COPY)]
            copy_b_n += [cla.add_node(BoolNodeTypes.COPY)]

            cla.add_input_node(copy_a_n[-1], f"a{i}")
            cla.add_input_node(copy_b_n[-1], f"b{i}")

        carry_lh, in_gen, in_pro, out_car, in_car = cls._build_carry_lookahead(n)

        shift_val = cla.parallel_shift(carry_lh)
        copy_a_n = [node_id + shift_val for node_id in copy_a_n]
        copy_b_n = [node_id + shift_val for node_id in copy_b_n]

        cla.iparallel(carry_lh)

        carry_in = cla.get_node_by_id(in_car).children[0]
        cla.remove_node_by_id(in_car)
        cla.add_input_node(carry_in, "c")

        for i in range(4 * n):
            g_n = cla.add_node(BoolNodeTypes.AND)
            cla.add_edge(copy_a_n[i], g_n)
            cla.add_edge(copy_b_n[i], g_n)

            p_n = cla.add_node(BoolNodeTypes.XOR)
            cla.add_edge(copy_a_n[i], p_n)
            cla.add_edge(copy_b_n[i], p_n)

            copy_p_n = cla.add_node(BoolNodeTypes.COPY)
            cla.add_edge(p_n, copy_p_n)

            generate_in = cla.get_node_by_id(in_gen[i]).children[0]
            propagate_in = cla.get_node_by_id(in_pro[i]).children[0]

            cla.remove_node_by_id(in_gen[i])
            cla.remove_node_by_id(in_pro[i])

            cla.add_edge(g_n, generate_in)
            cla.add_edge(copy_p_n, propagate_in)

            final_xor = cla.add_node(BoolNodeTypes.XOR)
            cla.add_edge(copy_p_n, final_xor)

            carry_out = cla.get_node_by_id(out_car[i]).parents[0]
            cla.remove_node_by_id(out_car[i])
            cla.add_edge(carry_out, final_xor)

            cla.add_output_node(final_xor, f"r{i}")

        cla.get_node_by_id(out_car[-1]).label = f"c'"

        return cla

    @classmethod
    def carry_lookahead_adder(cls, n: int) -> BoolCirc:
        """ Return the Adder circuit build with a Carry-Lookahead with register of size 4n.
        Inputs values are a, b. Output Values are r (the result) and c' (out carry)

        :param n: rank of the adder
        :return: The Beautiful Recursively Made Circuit
        """
        adder = cls.carry_lookahead_half_adder(n)
        adder._uncarryfy()
        return adder

    # endregion

    @classmethod
    def int_to_bool_circ(cls, val: int, register_size: int = 8) -> BoolCirc:
        """ create a bool circ, initialize output value to bit value of val
        :param val: number that determined the output value
        :param register_size: number of outputs
        :return: the bool circ with just outputs
        """
        bin_val = cls.to_bin(val, register_size)
        circ = cls.empty()

        for bit_id, bit_val in enumerate(bin_val[::-1]):
            n = circ.add_node(bit_val)
            circ.add_output_node(n, f"x{bit_id}")

        return circ

    # region Evaluate
    # region Evaluate Transforms (TD11.3)

    # Here you shall find any transformations about the third question of the eleventh TD

    def _copy_transform(self, copy_id: Id, primitive_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_copy_transform)")

        copy_node = self.get_node_by_id(copy_id)

        if copy_node.label != BoolNodeTypes.COPY:
            raise ValueError(f"Node {copy_id} is not a Copy Node (_copy_transform)")

        if primitive_id not in copy_node.parents:
            raise ValueError(f"Node {primitive_id} not a parent of Node {copy_id} "
                             f"(_copy_transform)")

        primitive_node = self.get_node_by_id(primitive_id)

        if primitive_node.label not in [BoolNodeTypes.FALSE, BoolNodeTypes.TRUE]:
            raise ValueError(f"Node {primitive_id} is not a primitive node (_copy_transform)")

        node_to_check = []

        for child_id, child_multiplicity in copy_node.children_ids.items():
            for _ in range(child_multiplicity):
                new_node = self.add_node(primitive_node.label)
                node_to_check += [new_node]
                self.add_edge(new_node, child_id)

        self.remove_node_by_id(primitive_id)
        self.remove_node_by_id(copy_id)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_copy_transform)")

        return node_to_check

    def _not_transform(self, not_id: Id, primitive_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_not_transform)")

        not_node = self.get_node_by_id(not_id)  # Node kept in Graph

        if not_node.label != BoolNodeTypes.NOT:
            raise ValueError(f"Node {not_id} is not a Not Node (_not_transform)")

        if primitive_id not in not_node.parents:
            raise ValueError(f"Node {primitive_id} not a parent of Node {not_id} "
                             f"(_not_transform)")

        primitive_node = self.get_node_by_id(primitive_id)

        if primitive_node.label == BoolNodeTypes.FALSE:
            not_node.label = BoolNodeTypes.TRUE
        elif primitive_node.label == BoolNodeTypes.TRUE:
            not_node.label = BoolNodeTypes.FALSE
        else:
            raise ValueError(f"Node {primitive_id} is not a primitive node (_not_transform)")

        self.remove_node_by_id(primitive_id)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_not_transform)")

        return [not_id]

    def _and_transform(self, and_id: Id, primitive_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_and_transform)")

        and_node = self.get_node_by_id(and_id)  # Node kept in Graph

        if and_node.label != BoolNodeTypes.AND:
            raise ValueError(f"Node {and_id} is not a And Node (_and_transform)")

        if primitive_id not in and_node.parents:
            raise ValueError(f"Node {primitive_id} not a parent of Node {and_id} "
                             f"(_and_transform)")

        primitive_node = self.get_node_by_id(primitive_id)

        if primitive_node.label == BoolNodeTypes.FALSE:
            for parent_id, parent_multiplicity in list(and_node.parents_ids.items()):
                if parent_id != primitive_id:
                    self.remove_parallel_edge(parent_id, and_id)

                    for _ in range(parent_multiplicity):
                        self.add_edge(parent_id, self.add_node(BoolNodeTypes.COPY))

            self.remove_node_by_id(primitive_id)
            and_node.label = BoolNodeTypes.FALSE

        elif primitive_node.label == BoolNodeTypes.TRUE:
            self.remove_node_by_id(primitive_id)
        else:
            raise ValueError(f"Node {primitive_id} is not a primitive node (_and_transform)")

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_and_transform)")

        return [and_id]

    def _or_transform(self, or_id: Id, primitive_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_or_transform)")

        or_node = self.get_node_by_id(or_id)  # Node kept in Graph

        if or_node.label != BoolNodeTypes.OR:
            raise ValueError(f"Node {or_id} is not a Or Node (_or_transform)")

        if primitive_id not in or_node.parents:
            raise ValueError(f"Node {primitive_id} not a parent of Node {or_id} "
                             f"(_or_transform)")

        primitive_node = self.get_node_by_id(primitive_id)

        if primitive_node.label == BoolNodeTypes.FALSE:
            self.remove_node_by_id(primitive_id)

        elif primitive_node.label == BoolNodeTypes.TRUE:
            for parent_id, parent_multiplicity in list(or_node.parents_ids.items()):
                if parent_id != primitive_id:
                    self.remove_parallel_edge(parent_id, or_id)

                    for _ in range(parent_multiplicity):
                        self.add_edge(parent_id, self.add_node(BoolNodeTypes.COPY))

            self.remove_node_by_id(primitive_id)
            or_node.label = BoolNodeTypes.TRUE

        else:
            raise ValueError(f"Node {primitive_id} is not a primitive node (_or_transform)")

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_or_transform)")

        return [or_id]

    def _xor_transform(self, xor_id: Id, primitive_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_xor_transform)")

        xor_node = self.get_node_by_id(xor_id)  # Node kept in Graph

        if xor_node.label != BoolNodeTypes.XOR:
            raise ValueError(f"Node {xor_id} is not a Xor Node (_xor_transform)")

        if primitive_id not in xor_node.parents:
            raise ValueError(f"Node {primitive_id} not a parent of Node {xor_id} "
                             f"(_xor_transform)")

        primitive_node = self.get_node_by_id(primitive_id)

        if primitive_node.label == BoolNodeTypes.TRUE:
            child_id = xor_node.children[0]
            self.remove_edge(xor_id, child_id)
            new_not_id = self.add_node(BoolNodeTypes.NOT)
            self.add_edge(xor_id, new_not_id)
            self.add_edge(new_not_id, child_id)

        self.remove_node_by_id(primitive_id)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_xor_transform)")

        return [xor_id]

    def _neutral_transform(self, node_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_neutral_transform)")

        node = self.get_node_by_id(node_id)

        if node.degree_in != 0:
            raise ValueError(f"Node {node_id} is not a upper leaf (_neutral_transform)")

        if node.label == BoolNodeTypes.AND:
            node.label = BoolNodeTypes.TRUE
        elif node.label == BoolNodeTypes.OR or node.label == BoolNodeTypes.XOR:
            node.label = BoolNodeTypes.FALSE
        else:
            raise ValueError(f"Node {node_id} don't have a neutral type (_neutral_transform)")

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_neutral_transform)")

        return [node_id]

    # endregion

    def get_var_list(self, target_var: str, get_input: bool) -> List[Node]:
        """ Get the list of input Node corresponding to the target Variable.
        Node list is in small endian  (Weak bit first)

        :param target_var: Variable to be found
        :param get_input: Boolean, True for finding an Input Var, False for an Output one
        :return: List of Node !
        """
        pos_to_node = {}

        if get_input:
            node_list = self.inputs_ids
        else:
            node_list = self.outputs_ids

        for node_id in node_list:
            var, pos = self.decompose_node_name(node_id)
            if var == target_var:
                pos_to_node[pos] = self.get_node_by_id(node_id)

        final_list = [None for _ in range(len(pos_to_node))]

        for node_num, node in pos_to_node.items():
            final_list[node_num] = node

        return final_list

    def set_input(self, input_var: str, input_circ: BoolCirc):
        """ Set the input `input_var` to the given circuit. (Check for compatibility before)

        :param input_var: Variable to be set
        :param input_circ: Circuit to be added.
        """
        input_nodes = self.get_var_list(input_var, get_input=True)

        if len(input_nodes) != len(input_circ.outputs_ids):
            raise ValueError(f"Input Circ incompatible with target variables.")

        for k, input_node in enumerate(input_nodes):
            output_node = input_circ.get_node_by_id(input_circ.outputs_ids[k])
            parent_node = input_circ.get_node_by_id(output_node.parents[0])
            input_node.label = parent_node.label
            self.inputs_ids.remove(input_node.id)

    def set_input_value(self, input_var: str, value: int):
        """ Set the specified input_var to the given value.
         Find the appropriate size of the register for you !

        :param input_var: Variable to be set
        :param value: Value Given
        """
        input_nodes = self.get_var_list(input_var, get_input=True)
        self.set_input(input_var, BoolCirc.int_to_bool_circ(value, len(input_nodes)))

    def get_output(self, output_var) -> int:
        """ Get the value of desired output
        :param output_var: Used Output
        :return: The Value ! (unsigned int)
        """
        output_nodes = self.get_var_list(output_var, get_input=False)

        final_value = 0
        power_of_two = 1

        for node in output_nodes:
            parent_node = self.get_node_by_id(node.parents[0])

            if parent_node.label == BoolNodeTypes.TRUE:
                final_value += power_of_two

            power_of_two *= 2

        return final_value

    def evaluate(self):
        """ Evaluate the bool circ """
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc before reduction (evaluate)")

        node_queue_id = self.leaf_list(lower_leaf=False, node_list=False)

        while node_queue_id:
            current_node_id = node_queue_id.pop(0)
            if current_node_id in self.nodes_ids:
                current_node = self.get_node_by_id(current_node_id)
                node_queue_id.extend(self._evaluation_step(current_node))

        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc after reduction (evaluate)")

    def _evaluation_step(self, current_node) -> List[Id]:
        """ Do a step of evaluation
        used in evaluate and simplify
        :param current_node: the current node
        :return: ids to check
        """
        if current_node.label in [BoolNodeTypes.TRUE, BoolNodeTypes.FALSE]:
            # current_node is a primitive
            child_node = self.get_node_by_id(current_node.children[0])

            if child_node.label == BoolNodeTypes.AND:
                return self._and_transform(child_node.id, current_node.id)

            elif child_node.label == BoolNodeTypes.OR:
                return self._or_transform(child_node.id, current_node.id)

            elif child_node.label == BoolNodeTypes.XOR:
                return self._xor_transform(child_node.id, current_node.id)

            elif child_node.label == BoolNodeTypes.COPY:
                return self._copy_transform(child_node.id, current_node.id)

            elif child_node.label == BoolNodeTypes.NOT:
                return self._not_transform(child_node.id, current_node.id)

            else:
                return []

        elif current_node.degree_in == 0 and current_node.label:
            # current_node is not a primitive and is an upper-leaf
            return self._neutral_transform(current_node.id)

        else:
            return []

    # endregion

    # region Encoder, Decoder, Simplification
    @classmethod
    def hamming_encoder(cls) -> BoolCirc:
        """ Implement hamming encoder for 4 bit
        encoded with 7 bit (3 of redundancy)
        Input is 'i', Output is 'o' for the data and 'r' for the added redundancy part
        :return: the bool circ of hamming encoder
        """
        circ = cls.parse("i0", "i1", "i2", "i3", "i0^i1^i2", "i0^i1^i3", "i0^i2^i3")

        for output_id in circ.outputs_ids:
            var, var_pos = circ.decompose_node_name(output_id)
            if var_pos < 4:
                circ.get_node_by_id(output_id).label = f"o{var_pos}"
            else:
                circ.get_node_by_id(output_id).label = f"r{var_pos - 4}"

        if not circ.is_well_formed():
            raise RuntimeError("Error during modification of circ...")

        return circ

    @classmethod
    def hamming_decoder(cls) -> BoolCirc:
        """implement hamming decoder for 7 bit (3 of redundancy)
        detect and correct one error.
        Input is 'i' for the data, 'r' for the added redundancy part and 'o'
        is the output of the decoder

        :return: the bool circ of hamming decoder
        """
        circ = cls.empty()
        in_copy = []
        for k in range(4):
            new_copy_id = circ.add_node(BoolNodeTypes.COPY)
            in_copy += [new_copy_id]
            circ.add_input_node(new_copy_id, f"i{k}")

        redundant_xor_0_id = circ.add_node(BoolNodeTypes.XOR)
        circ.add_edge(in_copy[0], redundant_xor_0_id)
        circ.add_edge(in_copy[1], redundant_xor_0_id)
        circ.add_edge(in_copy[2], redundant_xor_0_id)
        circ.add_input_node(redundant_xor_0_id, "r0")

        redundant_xor_1_id = circ.add_node(BoolNodeTypes.XOR)
        circ.add_edge(in_copy[0], redundant_xor_1_id)
        circ.add_edge(in_copy[1], redundant_xor_1_id)
        circ.add_edge(in_copy[3], redundant_xor_1_id)
        circ.add_input_node(redundant_xor_1_id, "r1")

        redundant_xor_2_id = circ.add_node(BoolNodeTypes.XOR)
        circ.add_edge(in_copy[0], redundant_xor_2_id)
        circ.add_edge(in_copy[2], redundant_xor_2_id)
        circ.add_edge(in_copy[3], redundant_xor_2_id)
        circ.add_input_node(redundant_xor_2_id, "r2")

        redundant_copy_id = [circ.add_node(BoolNodeTypes.COPY),
                             circ.add_node(BoolNodeTypes.COPY),
                             circ.add_node(BoolNodeTypes.COPY)]

        circ.add_edge(redundant_xor_0_id, redundant_copy_id[0])
        circ.add_edge(redundant_xor_1_id, redundant_copy_id[1])
        circ.add_edge(redundant_xor_2_id, redundant_copy_id[2])

        # 0
        output_and_0 = circ.add_node(BoolNodeTypes.AND)
        circ.add_edge(redundant_copy_id[0], output_and_0)
        circ.add_edge(redundant_copy_id[1], output_and_0)
        circ.add_edge(redundant_copy_id[2], output_and_0)

        output_xor_0 = circ.add_node(BoolNodeTypes.XOR)
        circ.add_edge(in_copy[0], output_xor_0)
        circ.add_edge(output_and_0, output_xor_0)
        circ.add_output_node(output_xor_0, "o0")

        # 1
        output_and_1 = circ.add_node(BoolNodeTypes.AND)

        circ.add_edge(redundant_copy_id[0], output_and_1)
        circ.add_edge(redundant_copy_id[1], output_and_1)

        tmp_not = circ.add_node(BoolNodeTypes.NOT)
        circ.add_edge(redundant_copy_id[2], tmp_not)
        circ.add_edge(tmp_not, output_and_1)
        del tmp_not

        output_xor_1 = circ.add_node(BoolNodeTypes.XOR)
        circ.add_edge(in_copy[1], output_xor_1)
        circ.add_edge(output_and_1, output_xor_1)
        circ.add_output_node(output_xor_1, "o1")

        # 2
        output_and_2 = circ.add_node(BoolNodeTypes.AND)

        circ.add_edge(redundant_copy_id[0], output_and_2)

        tmp_not = circ.add_node(BoolNodeTypes.NOT)
        circ.add_edge(redundant_copy_id[1], tmp_not)
        circ.add_edge(tmp_not, output_and_2)
        del tmp_not

        circ.add_edge(redundant_copy_id[2], output_and_2)

        output_xor_2 = circ.add_node(BoolNodeTypes.XOR)

        circ.add_edge(in_copy[2], output_xor_2)
        circ.add_edge(output_and_2, output_xor_2)
        circ.add_output_node(output_xor_2, "o2")

        # 3
        output_and_3 = circ.add_node(BoolNodeTypes.AND)

        tmp_not = circ.add_node(BoolNodeTypes.NOT)
        circ.add_edge(redundant_copy_id[0], tmp_not)
        circ.add_edge(tmp_not, output_and_3)
        del tmp_not

        circ.add_edge(redundant_copy_id[1], output_and_3)
        circ.add_edge(redundant_copy_id[2], output_and_3)

        output_xor_3 = circ.add_node(BoolNodeTypes.XOR)
        circ.add_edge(in_copy[3], output_xor_3)
        circ.add_edge(output_and_3, output_xor_3)
        circ.add_output_node(output_xor_3, "o3")

        if not circ.is_well_formed():
            raise RuntimeError("Error during modification of circ...")

        return circ

    # region Simplify Transforms (TD12.2)
    def _xor_associativity_transform(self, xor_id_down: Id, xor_id_up: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_xor_associativity_transform)")

        for parent_id, parent_multiplicity in self.get_node_by_id(xor_id_up).parents_ids.items():
            for _ in range(parent_multiplicity):
                self.add_edge(parent_id, xor_id_down)

        self.remove_node_by_id(xor_id_up)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_xor_associativity_transform)")

        return [xor_id_down]

    def _copy_associativity_transform(self, copy_id_down: Id, copy_id_up: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_copy_associativity_transform)")

        for child_id, child_multiplicity in self.get_node_by_id(copy_id_down).children_ids.items():
            for _ in range(child_multiplicity):
                self.add_edge(copy_id_up, child_id)

        self.remove_node_by_id(copy_id_down)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_copy_associativity_transform)")

        return [copy_id_up]

    def _xor_involution_transform(self, xor_id: Id, copy_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_xor_involution_transform)")

        copy_to_xor_edges_number = self.get_node_by_id(copy_id).children_ids[xor_id]

        self.remove_parallel_edge(copy_id, xor_id)

        if copy_to_xor_edges_number % 2 == 1:
            self.add_edge(copy_id, xor_id)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_xor_involution_transform)")

        return [xor_id, copy_id]

    def _delete_transform(self, delete_copy_id: Id) -> List[Id]:
        """ Used to delete a COPY when it has no child
        delete the node above if it is not a COPY
        :param delete_copy_id: the COPY node to delete
        :return: Id list of the node to check in the simplify process
        """
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_delete_transform)")

        # Copy's only parent
        parent_op_id = self.get_node_by_id(delete_copy_id).parents[0]
        parent_op_node = self.get_node_by_id(parent_op_id)

        ids_to_check = []

        if parent_op_node.label != BoolNodeTypes.COPY:
            for parent_id, parent_mult in parent_op_node.parents_ids.items():
                for _ in range(parent_mult):
                    new_delete_id = self.add_node(BoolNodeTypes.COPY)
                    self.add_edge(parent_id, new_delete_id)
                    ids_to_check.append(new_delete_id)

            self.remove_several_node_by_id([delete_copy_id, parent_op_id])
        else:
            self.remove_node_by_id(delete_copy_id)
            ids_to_check.append(parent_op_id)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_delete_transform)")

        return ids_to_check

    def _not_through_xor_transform(self, xor_id: Id, not_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_not_through_xor_transform)")

        # remove the first not
        self.add_edge(self.get_node_by_id(not_id).parents[0], xor_id)
        self.remove_node_by_id(not_id)

        xor_child_id = self.get_node_by_id(xor_id).children[0]
        new_not_id = self.add_node(BoolNodeTypes.NOT)

        # add a new not below the xor
        self.add_edge(xor_id, new_not_id)
        self.add_edge(new_not_id, xor_child_id)
        self.remove_edge(xor_id, xor_child_id)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_not_through_xor_transform)")

        return [new_not_id, xor_id]

    def _not_through_copy_transform(self, copy_id: Id, not_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_not_through_copy_transform)")

        not_parent_id = self.get_node_by_id(not_id).parents[0]
        self.add_edge(not_parent_id, copy_id)
        self.remove_node_by_id(not_id)

        ids_to_check = [copy_id]

        for child_id, child_multiplicity in list(self.get_node_by_id(copy_id).children_ids.items()):
            for _ in range(child_multiplicity):
                new_not_id = self.add_node(BoolNodeTypes.NOT)
                self.add_edge(new_not_id, child_id)
                self.add_edge(copy_id, new_not_id)
                self.remove_edge(copy_id, child_id)
                ids_to_check.append(new_not_id)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_not_through_copy_transform)")

        return ids_to_check

    def _not_involution_transform(self, not_up_id: Id, not_down_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_not_involution_transform)")

        self.add_edge(self.get_node_by_id(not_up_id).parents[0],
                      self.get_node_by_id(not_down_id).children[0])
        self.remove_several_node_by_id([not_up_id, not_down_id])

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_not_involution_transform)")

        return []

    # endregion

    # region Others Simplify Transforms (TD12.3)
    # We could do Morgan's Laws : (But not for today !)
    # - (~A) | (~B) = ~(A & B)
    # - (~A) & (~B) = ~(A | B)

    # or & and associativity transform are basically the same as xor associativity transform
    # same for or & and involution
    def _or_associativity_transform(self, or_id_down: Id, or_id_up: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_or_associativity_transform)")

        for parent_id, parent_multiplicity in self.get_node_by_id(or_id_up).parents_ids.items():
            for _ in range(parent_multiplicity):
                self.add_edge(parent_id, or_id_down)

        self.remove_node_by_id(or_id_up)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_or_associativity_transform)")

        return [or_id_down]

    def _and_associativity_transform(self, and_id_down: Id, and_id_up: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_and_associativity_transform)")

        for parent_id, parent_multiplicity in self.get_node_by_id(and_id_up).parents_ids.items():
            for _ in range(parent_multiplicity):
                self.add_edge(parent_id, and_id_down)

        self.remove_node_by_id(and_id_up)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_and_associativity_transform)")

        return [and_id_down]

    def _or_involution_transform(self, or_id: Id, copy_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_or_involution_transform)")

        # only one edge because A | A = A
        self.remove_parallel_edge(copy_id, or_id)
        self.add_edge(copy_id, or_id)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_or_involution_transform)")

        return [or_id, copy_id]

    def _and_involution_transform(self, and_id: Id, copy_id: Id) -> List[Id]:
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc (_and_involution_transform)")

        # only one edge because A & A = A
        self.remove_parallel_edge(copy_id, and_id)
        self.add_edge(copy_id, and_id)

        if not self.is_well_formed():
            raise ValueError(f"Error during transformation (_and_involution_transform)")

        return [and_id, copy_id]

    # endregion

    def _check_parents(self, node_id: Id) -> List[Id]:
        """ For AND, OR, XOR
        check if the node is necessary
        1 parent -> not needed
        :param node_id: the checked node id
        :return: Id's to check after call
        """
        if node_id in self.nodes_ids:
            node = self.get_node_by_id(node_id)
            if len(node.parents) == 1 and node.parents_ids[node.parents[0]] == 1:
                parent_node_id = node.parents[0]
                child_node_id = node.children[0]
                self.add_edge(parent_node_id, child_node_id)
                self.remove_node_by_id(node_id)

                to_check = []

                if parent_node_id not in self.inputs_ids:
                    to_check.append(parent_node_id)

                if child_node_id not in self.outputs_ids:
                    to_check.append(child_node_id)

                return to_check

        return []

    def simplify(self):
        """ Simplify self
        ... on est désolé Monsieur, cette fonction est atroce ... bon courage ..
         On est dispo par mail si vous voulez de la lumière :)

         Pour les stats :
          - On supprime environ 35% des portes sur des circuits aléatoirement générés.
          - Médiane 38%, Écart Type 26%.

         Test effectué sur 100 circuits d'environ 80 nœuds chacun avec 5 entrées et 5 sorties. """
        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc before simplification (simplify)")

        node_queue_id = []  # nodes to be checked

        def modification_made(to_add_to_queue: List[Id]) -> bool:
            """return True if a modification has been made and add to_add_to_queue to queue
            (pretty obvious, innit?)
            :param to_add_to_queue: to be added to the queue
            (still obvious (we are talented for obvious names))
            :return: a modification has been made on self
            """
            if to_add_to_queue:  # if more nodes need to be checked, a modification has been made
                node_queue_id.extend(to_add_to_queue)
                return True
            else:
                return False

        has_made_modifications = True

        while has_made_modifications:
            node_queue_id = [node.id for node in self.middle_nodes]
            has_made_modifications = False

            while node_queue_id:
                current_node_id = node_queue_id.pop()

                if current_node_id in self.nodes_ids and \
                        current_node_id not in self.inputs_ids and \
                        current_node_id not in self.outputs_ids:
                    current_node = self.get_node_by_id(current_node_id)

                    has_made_modifications |= modification_made(self._evaluation_step(current_node))

                    # we check every node that can be modified
                    if current_node.label == BoolNodeTypes.COPY:
                        if not current_node.children:
                            has_made_modifications |= modification_made(
                                self._delete_transform(current_node_id))
                        else:
                            if len(current_node.children) == 1 \
                                    and current_node.children_ids[current_node.children[0]] == 1:
                                has_made_modifications |= modification_made(
                                    self._delete_copy(current_node_id))
                            else:
                                for child_id, child_multiplicity in list(
                                        current_node.children_ids.items()):
                                    child_node = self.get_node_by_id(child_id)

                                    if child_node.label == BoolNodeTypes.COPY:
                                        has_made_modifications |= modification_made(
                                            self._copy_associativity_transform(child_id,
                                                                               current_node_id))

                                    elif child_node.label == BoolNodeTypes.XOR \
                                            and child_multiplicity > 1:
                                        has_made_modifications |= modification_made(
                                            self._xor_involution_transform(child_id,
                                                                           current_node_id))

                                    elif child_node.label == BoolNodeTypes.OR \
                                            and child_multiplicity > 1:
                                        has_made_modifications |= modification_made(
                                            self._or_involution_transform(child_id,
                                                                          current_node_id))

                                    elif child_node.label == BoolNodeTypes.AND \
                                            and child_multiplicity > 1:
                                        has_made_modifications |= modification_made(
                                            self._and_involution_transform(child_id,
                                                                           current_node_id))

                    elif current_node.label == BoolNodeTypes.XOR:
                        child_id = current_node.children[0]
                        if self.get_node_by_id(child_id).label == BoolNodeTypes.XOR:
                            has_made_modifications |= modification_made(
                                self._xor_associativity_transform(child_id, current_node_id))

                        has_made_modifications |= modification_made(
                            self._check_parents(current_node_id))

                    elif current_node.label == BoolNodeTypes.NOT:
                        child = self.get_node_by_id(current_node.children[0])

                        if child.label == BoolNodeTypes.XOR:
                            has_made_modifications |= modification_made(
                                self._not_through_xor_transform(child.id, current_node_id))

                        elif child.label == BoolNodeTypes.COPY:
                            has_made_modifications |= modification_made(
                                self._not_through_copy_transform(child.id, current_node_id))

                        elif child.label == BoolNodeTypes.NOT:
                            has_made_modifications |= modification_made(
                                self._not_involution_transform(current_node_id, child.id))

                    elif current_node.label == BoolNodeTypes.OR:
                        child_id = current_node.children[0]
                        if self.get_node_by_id(child_id).label == BoolNodeTypes.OR:
                            has_made_modifications |= modification_made(
                                self._or_associativity_transform(child_id, current_node_id))

                        has_made_modifications |= modification_made(
                            self._check_parents(current_node_id))

                    elif current_node.label == BoolNodeTypes.AND:
                        child_id = current_node.children[0]
                        if self.get_node_by_id(child_id).label == BoolNodeTypes.AND:
                            has_made_modifications |= modification_made(
                                self._and_associativity_transform(child_id, current_node_id))

                        has_made_modifications |= modification_made(
                            self._check_parents(current_node_id))

        if not self.is_well_formed():
            raise ValueError(f"Invalid BoolCirc after simplification (simplify)")

    def _delete_copy(self, copy_id: Id) -> List[Id]:
        """ For COPY only, called when copy has only one child -> a useless copy

        :param copy_id: the node to delete """
        node = self.get_node_by_id(copy_id)
        to_check = [node.parents[0], node.children[0]]

        self.add_edge(node.parents[0], node.children[0])
        self.remove_node_by_id(copy_id)
        return to_check
    # endregion
