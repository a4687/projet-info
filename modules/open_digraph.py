from __future__ import annotations

from enum import IntFlag, auto
from random import choice
from typing import Dict, List, Tuple, Union
import re

import modules.random_matrix as random_matrix
from modules.node import Id, Node


class DigraphType(IntFlag):
    """ Simple Enum to specify the Digraph Types : Free, LoopFree, Acyclic, Oriented, Undirected

    Free : No Constraints
    LoopFree : Each node cannot be linked to itself
    Acyclic : Not any cycle in graph
    Oriented : Each node A is linked to another one B in only one direction (A -> B xor B -> A)
    Undirected : Not Oriented

    Exemple:
      - For a LoopFree Digraph:
        d = OpenDigraph.random(... form=DigraphType.LoopFree ...)
      - For an Acyclic Digraph:
        d = OpenDigraph.random(... form=DigraphType.Acyclic ...)
      - For an Acyclic LoopFree Digraph:
        d = OpenDigraph.random(... form=DigraphType.Acyclic | DigraphType.LoopFree ...)
      - For an Undirected LoopFree Digraph:
        d = OpenDigraph.random(... form=DigraphType.Undirected | DigraphType.LoopFree ...)
    """
    Free = auto()
    LoopFree = auto()
    Acyclic = auto()
    Oriented = auto()
    Undirected = auto()


class OpenDigraph:
    """ OpenDigraph Class """

    def __init__(self, inputs: List[Id], outputs: List[Id], nodes: List[Node]):
        """
        :param inputs: ids of the input nodes
        :param outputs: ids of the output nodes
        :param nodes: node iterable (ex. list) """
        self._inputs = inputs
        self._outputs = outputs
        self._nodes = {node.id: node for node in nodes}  # Dict[Id, Node]

        # _max : Id. Maximum of Digraph Id, used to find an unused Id
        self._max = max(list(self._nodes.keys()), default=None)

    def __str__(self) -> str:
        txt = f"OpenDigraph (input: {self.inputs_ids}, output: {self.outputs_ids})\n"
        for node in self.nodes:
            txt += f"\t - {node}\n"

        return txt

    def __repr__(self) -> str:
        return str(self)

    @classmethod
    def empty(cls):
        """ Create an empty Digraph.

        :return: Empty Digraph """
        return cls([], [], [])

    def copy(self):
        """ Return a new instance of a Digraph.

        :return: new and independent Digraph """
        return OpenDigraph(self.inputs_ids.copy(), self.outputs_ids.copy(),
                           [node.copy() for node in self.nodes])

    @classmethod
    def random(cls, n: int, bound: int, form: DigraphType, inputs_nb: int = 0, outputs_nb: int = 0):
        """Create a random digraph.

        :param n: Number of Nodes
        :param bound: Max bound per Node
        :param form: Form of desired digraph (See DigraphType)
        :param inputs_nb: number of input
        :param outputs_nb: number of output
        :return: Desired OpenDigraph """
        is_loop_free = bool(form & DigraphType.LoopFree)
        if is_loop_free:
            form -= DigraphType.LoopFree

        if form == DigraphType.Free:
            d = cls.graph_from_adjacency_matrix(
                random_matrix.random_int_matrix(n, bound, null_diag=is_loop_free))

        elif form == DigraphType.Acyclic:
            if is_loop_free:
                raise ValueError("Redondant type : Acyclic Graph is LoopFree Graph")
            d = cls.graph_from_adjacency_matrix(
                random_matrix.random_triangular_int_matrix(n, bound, null_diag=True))

        elif form == DigraphType.Oriented:
            d = cls.graph_from_adjacency_matrix(
                random_matrix.random_oriented_int_matrix(n, bound, null_diag=is_loop_free))

        elif form == DigraphType.Undirected:
            d = cls.graph_from_adjacency_matrix(
                random_matrix.random_symmetric_int_matrix(n, bound, null_diag=is_loop_free))

        elif is_loop_free:
            d = cls.graph_from_adjacency_matrix(
                random_matrix.random_int_matrix(n, bound, null_diag=True))

        else:
            raise ValueError("Unavailable configuration for a DigraphType")

        i = 0
        for child_id in random_matrix.random_int_list(inputs_nb, n - 1):
            d.add_input_node(child_id, f"Input {i}")
            i += 1

        i = 0
        for parent_id in random_matrix.random_int_list(outputs_nb, n - 1):
            d.add_output_node(parent_id, f"Output {i}")
            i += 1

        return d

    # region Inputs
    @property
    def inputs_ids(self) -> List[Id]:
        """ Return Input Id List.

        :return: Input Id List """
        return self._inputs

    @inputs_ids.setter
    def inputs_ids(self, new_inputs_ids: List[Id]):
        """ Set new Input Id List.

        :param new_inputs_ids: New Input ID List """
        self._inputs = new_inputs_ids

    def add_input_id(self, input_id: Id):
        """ Add input_id to Input Id List. DO NOT USE -> Prefer add_input_node.

        :param input_id: Id to add """
        self.inputs_ids += [input_id]

    def add_input_node(self, child_id: Id, label: str = "") -> Id:
        """ Create a new Input Node.

        :param child_id: Child of new Input Node
        :param label: Input Node Label """
        if child_id in self.inputs_ids:
            raise ValueError(f"Cannot add a parent to the input node (ID : {child_id}.)")

        new_id = self.add_node(label)
        self.inputs_ids += [new_id]
        self.add_edge(new_id, child_id)

        return new_id

    # endregion

    # region Outputs
    @property
    def outputs_ids(self) -> List[Id]:
        """ Return Output Id List.

        :return: Output Id List """
        return self._outputs

    @outputs_ids.setter
    def outputs_ids(self, new_outputs_ids: List[Id]):
        """ Set new Output Id List.

        :param new_outputs_ids: New Output ID List """
        self._outputs = new_outputs_ids

    def add_output_id(self, output_id: Id):
        """ Add output_id to Output Id List. DO NOT USE -> Prefer add_output_node.

        :param output_id: Id to add """
        self.outputs_ids += [output_id]

    def add_output_node(self, parent_id: Id, label: str = "") -> Id:
        """ Create a new Output Node.

        :param parent_id: Parent of new Output Node
        :param label: Output Node Label """
        if parent_id in self.outputs_ids:
            raise ValueError(f"Cannot add a child to the output node (ID : {parent_id}.")

        new_id = self.add_node(label)
        self.outputs_ids += [new_id]
        self.add_edge(parent_id, new_id)

        return new_id

    # endregion

    # region Nodes
    @property
    def node_map(self) -> Dict[Id, Node]:
        """ Dictionary witch maps a Node ID to a Node.

        :return: Dictionary (Id -> Node) """
        return self._nodes

    @property
    def nodes(self) -> List[Node]:
        """ Return Node List.

        :return: Node List """
        return list(self.node_map.values())

    @property
    def nodes_ids(self) -> List[Id]:
        """ Return Node Id List.

        :return: Node Id List """
        return list(self.node_map)

    def get_node_by_id(self, id: Id) -> Node:
        """ Return Node from specified Id.

        :param id: Id of desired Node
        :return: Node Class """
        return self.node_map[id]

    def get_nodes_by_ids(self, ids: List[Id]) -> List[Node]:
        """ Return Node List from specified Id List.

        :param ids: Id List of desired Nodes
        :return: Node Class List """
        return [self.get_node_by_id(i) for i in ids]

    def add_node(self, label: str = "", parents: Dict[Id, int] = None,
                 children: Dict[Id, int] = None) -> Id:
        """ Add a new Node to Digraph.

        :param label: Node label
        :param parents: Parent Dictionary
        :param children: Children Dictionary
        :return: Created Node Id """

        if parents is None:
            parents = {}

        if children is None:
            children = {}

        new_id = self.new_id()
        self.node_map[new_id] = Node(label, new_id, {}, {})

        for parent_id, mult in parents.items():
            for _ in range(mult):
                self.add_edge(parent_id, new_id)

        for child_id, mult in children.items():
            for _ in range(mult):
                self.add_edge(new_id, child_id)

        return new_id

    def remove_node_by_id(self, node_id: Id):
        """ Remove Node from his Id. Remove all edge from and to this node.

        :param node_id: Node Id to remove """
        node_to_delete = self.get_node_by_id(node_id)

        for parent in node_to_delete.parents:
            self.remove_parallel_edge(parent, node_id)

        for child in node_to_delete.children:
            self.remove_parallel_edge(node_id, child)

        if node_id in self.inputs_ids:
            self.inputs_ids.remove(node_id)

        if node_id in self.outputs_ids:
            self.outputs_ids.remove(node_id)

        self.node_map.pop(node_id)

    def remove_several_node_by_id(self, list_src_target: List[Id]):
        """ Remove Nodes from Id List. Remove all edge from and to deleted Nodes.

        :param list_src_target: Node Id List to remove """
        for node in list_src_target:
            self.remove_node_by_id(node)

    def merge(self, node_id_1: Id, node_id_2: Id, new_label: str = None):
        """ Merge Node 2 into Node 1

        :param node_id_1: Node used to Merge
        :param node_id_2: Node Merged
        :param new_label: New label for merged node (if None, keep Node 1 label)
        :return: """
        node1 = self.get_node_by_id(node_id_1)
        node2 = self.get_node_by_id(node_id_2)

        if new_label is not None:
            node1.label = new_label  # If None, we keep node_1's label

        # Keep node_id_1, Delete node_id_2

        for child_id, child_multiplicity in node2.children_ids.items():
            for _ in range(child_multiplicity):
                self.add_edge(node_id_1, child_id)

        for parent_id, parent_multiplicity in node2.parents_ids.items():
            for _ in range(parent_multiplicity):
                self.add_edge(parent_id, node_id_1)

        self.remove_node_by_id(node_id_2)

    @property
    def middle_nodes(self) -> List[Node]:
        """ Return all the node not in input_ids and not in output_id
        :return: List[Node]
        """
        return self.get_nodes_by_ids(
            set(self.nodes_ids) - set(self.outputs_ids) - set(self.inputs_ids))

    # endregion

    # region Edges
    def add_edge(self, source: Id, target: Id):
        """ Add an edge from source to target.

        :param source: Source Node Id
        :param target: Target Node ID """
        self.node_map[source].add_child_id(target)
        self.node_map[target].add_parent_id(source)

    def remove_edge(self, source: Id, target: Id):
        """ Add an edge from source to target.

        :param source: Source Node Id
        :param target: Target Node Id """
        self.node_map[target].remove_parent_once(source)
        self.node_map[source].remove_child_once(target)

    def remove_several_edges(self, list_src_target: List[Tuple[Id, Id]]):
        """ Remove edges from each pair (source, target).

        :param list_src_target: List of Pair (source, target) """
        for source, target in list_src_target:
            self.remove_edge(source, target)

    def remove_parallel_edge(self, source: Id, target: Id):
        """ Remove all edges from source to target.

        :param source: Source Node Id
        :param target: Target Node Id """
        self.node_map[target].remove_parent_id(source)
        self.node_map[source].remove_child_id(target)

    def remove_several_parallel_edge(self, list_src_target: List[Tuple[Id, Id]]):
        """ Remove all edges from each pair (source, target).

        :param list_src_target: List of Pair (source, target) """
        for source, target in list_src_target:
            self.remove_parallel_edge(source, target)

    # endregion

    # region Digraph Types
    def is_well_formed(self) -> bool:
        """ Test if Digraph is well-formed.

        :return: True if well-formed, False otherwise """
        for node_id, node in self.node_map.items():  # Let's check all nodes
            if not (isinstance(node, Node) and node_id == node.id):  # key != node id's -> Error
                return False

            for child in node.children:
                if not (child in self.nodes_ids and
                        node_id in self.node_map[child].parents_ids and
                        node.children_ids[child] == self.node_map[child].parents_ids[node_id]):
                    return False  # child doesn't have node_id as parent OR multiplicity != -> Error

            for parent in node.parents:
                if not (parent in self.nodes_ids and
                        node_id in self.node_map[parent].children_ids and
                        node.parents_ids[parent] == self.node_map[parent].children_ids[node_id]):
                    return False  # parent doesn't have node_id as child OR multiplicity != -> Error

        for node_id in self.inputs_ids:
            if not (node_id in self.nodes_ids and  # node_id is in nodes_ids
                    len(self.node_map[node_id].children_ids) == 1 and  # node has only one child
                    len(self.node_map[node_id].parents_ids) == 0 and  # node has no parent
                    self.node_map[node_id].children_ids[
                        self.node_map[node_id].children[0]] == 1):  # child multiplicity is 1
                return False

        for node_id in self.outputs_ids:
            if not (node_id in self.nodes_ids and  # node_id is in nodes_ids
                    len(self.node_map[node_id].children_ids) == 0 and  # node has no child
                    len(self.node_map[node_id].parents_ids) == 1 and  # node has only one parent
                    self.node_map[node_id].parents_ids[
                        self.node_map[node_id].parents[0]] == 1):  # parent multiplicity is 1
                return False

        return True

    def leaf_list(self, lower_leaf: bool = True, node_list: bool = True) -> List[Union[Node, Id]]:
        """ Return All Leaf of given OpenDigraph

        :param lower_leaf: True if we want leaves without children (at the bottom)
        False if we want leaves without parents (at the top)
        :param node_list: True if we want a Node List, False for an Id List
        :return: Leaf List """

        rep = []

        if lower_leaf:
            for n in self.nodes:
                if not n.children:
                    if node_list:
                        rep += [n]
                    else:
                        rep += [n.id]
        else:
            for n in self.nodes:
                if not n.parents:
                    if node_list:
                        rep += [n]
                    else:
                        rep += [n.id]

        return rep

    def is_cyclic(self) -> bool:
        """ Test if digraph is cyclic.

        :return: True if Cyclic, False otherwise """

        g = self.copy()

        llist = g.leaf_list()
        while llist:
            for leaf in llist:
                g.remove_node_by_id(leaf.id)
            llist = g.leaf_list()

        return bool(g.nodes)  # If g.nodes != [] -> Cyclic Else Acyclic

    # endregion

    # region Ids manipulations
    def new_id(self) -> Id:
        """ Return an unused Id.

        :return: Unused Id """
        if self._max is None:
            self._max = 0
        else:
            self._max += 1
        return Id(self._max)

    def id_to_int(self) -> Dict[Id, int]:
        """ Maps a Node Id to an Integer in 0 <= Number of Nodes.

        :return: Dictionary (Id -> int) """
        L = self.middle_nodes
        return {L[i].id: i for i in range(len(L))}

    def min_id(self) -> Id:
        return min(self.nodes_ids) if self.nodes_ids != [] else Id(0)

    def max_id(self) -> Id:
        return self._max if self._max is not None else Id(0)

    def shift_indices(self, n: int):
        new_dict = {}
        for node in self.node_map.values():
            node.id += n
            node.parents_ids = {p_id + n: p_mult for p_id, p_mult in node.parents_ids.items()}
            node.children_ids = {c_id + n: p_mult for c_id, p_mult in node.children_ids.items()}
            new_dict[node.id] = node

        self._nodes = new_dict
        self.inputs_ids = [in_id + n for in_id in self.inputs_ids]
        self.outputs_ids = [out_id + n for out_id in self.outputs_ids]

        if self._max is None:
            self._max = n
        else:
            self._max += n

    # endregion

    # region Matrix and Dot File
    @classmethod
    def graph_from_adjacency_matrix(cls, matrix: random_matrix.IntMatrix) -> OpenDigraph:
        """ Create Digraph from Adjacency Matrix

        :param matrix: Used Adjacency Matrix
        :return: New Graph """
        d = cls.empty()
        n = len(matrix)

        for name in range(n):
            d.add_node(f"{name}")  # Name == Id

        for i in range(n):
            for j in range(n):
                for _ in range(matrix[i][j]):  # Add multiplicity
                    d.add_edge(i, j)

        return d

    def adjacency_matrix(self) -> random_matrix.IntMatrix:
        """ Create Adjacency Matrix of Digraph

        :return: Adjacency Matrix """
        L = self.id_to_int()
        n = len(L)
        M = random_matrix.empty_matrix(n)

        for node_id, unique_id in L.items():
            # for all nodes which are not outputs or inputs

            for child_id, mult in self.node_map[node_id].children_ids.items():
                # for all its children and their multiplicity

                if child_id in L:  # Otherwise, child_id is an output node -> Ignored
                    M[unique_id][L[child_id]] = mult

        return M

    def save_as_dot_file(self, path: str, verbose: bool = False):
        """Save digraph on `path` file.

        :param path: File Path into save Digraph
        :param verbose: Print more information """

        with open(path, 'w') as f:  # Let's open file
            f.write("digraph {\n\n\t/* Nodes Definitions */\n")  # Digraph declaration & Header

            # We iterate on each Node
            for node_id in self._nodes:
                # If not an Input or Output Node
                if node_id not in self._inputs and node_id not in self._outputs:
                    node_label = self.get_node_by_id(node_id).label
                    # We write its declaration
                    if verbose:
                        f.write(f"\t{node_id} [label=\"{node_label} (id : {node_id})\","
                                f" shape=circle];\n")
                    else:
                        f.write(f"\t{node_id} [label=\"{node_label}\", shape=circle];\n")

            f.write("\n\t/* Inputs Definitions */\n")  # Input Header

            # We iterate on each Input Node
            for input_id in self._inputs:
                node_label = self.get_node_by_id(input_id).label
                # We write its declaration
                if verbose:
                    f.write(f"\t{input_id} [label=\"{node_label} (id : {input_id})\", type=input,"
                            f" shape=diamond];\n")
                else:
                    f.write(f"\t{input_id} [label=\"{node_label}\", type=input, shape=diamond];\n")

            f.write("\n\t/* Output Definitions */\n")  # Output Header

            # We iterate on each Output Node
            for output_id in self._outputs:
                node_label = self.get_node_by_id(output_id).label
                # We write its declaration
                if verbose:
                    f.write(f"\t{output_id} [label=\"{node_label} (id : {output_id})\", "
                            f"type=output, shape=rectangle];\n")
                else:
                    f.write(f"\t{output_id} [label=\"{node_label}\", "
                            f"type=output, shape=rectangle];\n")

            f.write("\n\t/* Links Definitions */\n")  # Links Header

            # We iterate on each Node
            for source_id, source_node in self.node_map.items():
                for child_id, child_mult in source_node.children_ids.items():
                    for _ in range(child_mult):
                        f.write(f"\t{source_id} -> {child_id};\n")
                    f.write("\n")

            f.write("}\n")  # We close Digraph Definition

    def display(self, verbose: bool = False):
        """ Display Digraph with available software programs.

        :param verbose: Print more information """

        # Creation of temporary directory
        from tempfile import mkdtemp
        tmp_path = mkdtemp()

        # Creation of unique filename (based on time at execution)
        from datetime import datetime
        tmp_dot_file = f"{tmp_path}/{datetime.now().isoformat()}.dot"
        tmp_pdf_file = f"{tmp_path}/{datetime.now().isoformat()}.pdf"

        # And we save the digraph into the Dot File
        self.save_as_dot_file(tmp_dot_file, verbose)

        from shutil import which

        # Check if `dot` program is installed
        if which("dot"):
            from sys import platform
            from subprocess import call

            # If installed, we create a pdf from Dot File
            call(["dot", "-Tpdf", tmp_dot_file, "-o", tmp_pdf_file])

            # tLet's now open the PDF File
            if platform == "win32":
                from os import startfile
                startfile(tmp_pdf_file)  # On Windows, we use `os.startfile`
            else:
                if platform == "darwin":
                    call(["open", tmp_pdf_file])  # On Mac, we use `open` commend
                else:
                    call(["xdg-open", tmp_pdf_file])  # On Mac, we use `xdg-open` commend

        else:
            # If `dot` isn't installed, we use a website for the preview
            with open(tmp_dot_file, 'r') as f:
                buffer = f.read()

            # `webbrowser` module auto-detects available browsers and launch the preferred one.
            # No escape to do, the module does it for uss :)
            import webbrowser
            webbrowser.open(f"https://dreampuf.github.io/GraphvizOnline/#{buffer}")

    @classmethod
    def from_dot_file(cls, path: str) -> OpenDigraph:
        """ Parse a Dot File to create correspondant Digraph.

        :param path: File Path to read
        :return: New Digraph """
        d = cls.empty()

        dot_id_2_node_id = {}  # Maps the Id (str) of Nodes in Dot file to the Id (int) in Digraph
        in_digraph_struct = False

        # Detect the start of a Digraph Struct
        digraph_begin_regex = re.compile("^\s*digraph\s*{\s*$")
        # Detect the end of a Digraph Struct
        digraph_end_regex = re.compile("^\s*}[^;]*$")

        # Detect a Node Declaration. Ex : nodeId [label="The Label", type=input/output, ...];
        node_regex = re.compile("^\s*(?P<node_id>\w+)\s*\[label=\"(?P<label>[^()\ ]*).*\"("
                                "|,\s*type=(?P<type>input|output)|,\s*[^],]*)*];\s*$")

        # Detect a List of Links. Ex : nodeID -> {child1, child2, ...};
        list_link_regex = re.compile("^\s*(?P<source_id>\w+)\s*->\s*{(?P<target_ids>(?:\w+\s*)*)}"
                                     "\s*;$")
        # Detect a linked link list. Ex : node1 -> node2 -> node3 -> node4; [Or] node -> child;
        linked_link_regex = re.compile("^\s*(?P<id_list>\w+(\s*->\s*\w+)*);$")

        with open(path, 'r') as f:
            # While we're not in a Digraph struct, we keep looking...
            while not in_digraph_struct:
                in_digraph_struct = bool(digraph_begin_regex.match(f.readline()))

            # While we're in the struct, we parse !
            while in_digraph_struct:
                line = f.readline()

                if line == "":  # -> End of file without closed Digraph struct
                    raise RuntimeError("Unclosed digraph declaration")

                if digraph_begin_regex.match(line):
                    # A Digraph struct in a Digraph Struct -> Impossible
                    raise RuntimeError("Stacked digraph declaration")

                node_declaration_match = node_regex.match(line)
                # If a Node Declaration is Detected
                if node_declaration_match:
                    # We extract useful information (`node_id`, `label` & `type`)
                    node_dot_id = node_declaration_match.group('node_id')
                    node_label = node_declaration_match.group('label')
                    node_type = node_declaration_match.group('type')

                    # If Node doesn't exist in Digraph
                    if node_dot_id not in dot_id_2_node_id:
                        # We create it !
                        dot_id_2_node_id[node_dot_id] = d.add_node()

                    node_dot_id = dot_id_2_node_id[node_dot_id]  # The Id of Node in Digraph

                    # We change the label
                    d.get_node_by_id(node_dot_id).label = node_label

                    if node_type == "input":
                        # If Node is an input, we add it to the input list
                        d.add_input_id(node_dot_id)
                    elif node_type == "output":
                        # If Node is an output, we add it to the output list
                        d.add_output_id(node_dot_id)

                list_link_detected = list_link_regex.match(line)
                # If a List of Link is Detected
                if list_link_detected:
                    # We extract the `source_id`
                    source_id = list_link_detected.group('source_id')
                    # We find all Ids from the `target_ids` string
                    target_list = re.findall("\w+", list_link_detected.group('target_ids'))

                    # If Source Node doesn't exist in Digraph, we add it.
                    if source_id not in dot_id_2_node_id:
                        dot_id_2_node_id[source_id] = d.add_node()

                    # We iterate on each Dot Ids
                    for target_dot_id in target_list:
                        # If Target Node doesn't exist in Digraph, we add it.
                        if target_dot_id not in dot_id_2_node_id:
                            dot_id_2_node_id[source_id] = d.add_node()

                        # We finally add the new edge !
                        d.add_edge(dot_id_2_node_id[source_id], dot_id_2_node_id[target_dot_id])

                linked_link_detected = linked_link_regex.match(line)
                # If a Liked of Link is Detected
                if linked_link_detected:
                    # We extract all Ids of the linked list. Ex : n1 -> n2 -> n3 became [n1, n2, n3]
                    all_ids = re.findall("\w+", linked_link_detected.group('id_list'))

                    # We check if the First Node exits in Digraph. Otherwise, we add it.
                    if all_ids[0] not in dot_id_2_node_id:
                        dot_id_2_node_id[all_ids[0]] = d.add_node()

                    # Now we iterate over all others Ids
                    for i in range(1, len(all_ids)):
                        source_dot_id = all_ids[i - 1]  # Source Dot Id is the previous Node.
                        # Source Node exits in Digraph, it has been checked in the previous loop
                        target_dot_id = all_ids[i]  # Target Dot Id is the actual Node

                        # We check if the Target Node exits in Digraph. Otherwise, we add it.
                        if target_dot_id not in dot_id_2_node_id:
                            dot_id_2_node_id[target_dot_id] = d.add_node()

                        # We finally add the new edge.
                        d.add_edge(dot_id_2_node_id[source_dot_id], dot_id_2_node_id[target_dot_id])

                if digraph_end_regex.match(line):
                    # End struct Detected, we break the loop -> Parsing complete !
                    in_digraph_struct = False

        # Finally, we return the fresh Digraph ! (We do not work for free...)
        return d

    # endregion

    # region Composition

    def parallel_shift(self, digraph: OpenDigraph) -> int:
        """ return the shift that should be applied on the node ids of my self to add safely diagraph

        :param digraph: digraph that will be applied on myself
        :return: the shift (int)
        """
        return digraph.max_id() - self.min_id() + 1

    def iparallel(self, g: Union[OpenDigraph, List[OpenDigraph]]):
        """ add the digraph or the digraph list to myself
        :param g: OpenDigraph or OpenDigraph List to be added
        """
        if isinstance(g, list):
            for digraph in g:
                self.shift_indices(self.parallel_shift(digraph))

                for node in digraph.nodes:
                    self.node_map[node.id] = node.copy()

                self._inputs += digraph.inputs_ids.copy()
                self._outputs += digraph.outputs_ids.copy()
        else:
            self.iparallel([g])

    @classmethod
    def parallel(cls, digraph_list: List[OpenDigraph]) -> OpenDigraph:
        """ return a brand new Digraph of myself with digraph_List added

        :param digraph_list:
        :return: a new open Digraph
        """
        d = cls.empty()
        d.iparallel(digraph_list)
        return d

    def icompose(self, g: OpenDigraph):  # g above self
        """ Add g on top of myself, matching his outputs and my input
        :param g: the digraph to be added on top
        """
        if len(self.inputs_ids) != len(g.outputs_ids):
            raise ValueError(f"Other must have {len(self.inputs_ids)} outputs nodes.")

        self.shift_indices(g.max_id() - self.min_id() + 1)

        for node in g.nodes:
            self.node_map[node.id] = node.copy()

        for input_id, output_id in zip(self.inputs_ids, g.outputs_ids):
            output_parent_id = g.get_node_by_id(output_id).parents[0]
            input_child_id = self.get_node_by_id(input_id).children[0]

            self.add_edge(output_parent_id, input_child_id)

        self.remove_several_node_by_id(self.inputs_ids + g.outputs_ids)
        self.inputs_ids = g.inputs_ids.copy()

    @classmethod
    def compose(cls, f: OpenDigraph, g: OpenDigraph) -> OpenDigraph:  # f above g
        """ return a brand new open digraph with f above g, matching f output and g inputs
        :param f: digraph on top
        :param g: digraph below
        :return: f above g
        """
        d = cls.empty()
        d.icompose(g)
        d.icompose(f)
        return d

    def connected_components(self) -> Tuple[int, Dict[Id, int]]:
        """ give the same color to neighbor nodes (connected component)
        :return: number of color used and a dict matching each node id with its color
        """
        color = 0
        id_to_con = {} #id to connect

        # While all node are not in id_to_con
        while len(id_to_con) < len(self.nodes):
            # We pick a random node which is not in any components
            random_id = choice(self.nodes_ids)
            while random_id in id_to_con:
                random_id = choice(self.nodes_ids)

            # Initialisation of lists
            to_visit = [random_id]
            yet_visited = []

            # While we keep having some node to visit
            while to_visit:
                # We extract the last node and remove it from the list to visit
                the_node = self.node_map[to_visit.pop()]
                id_to_con[the_node.id] = color  # Set the color
                yet_visited += [the_node.id]  # No need to visit it anymore

                # We mark all his children to visit if not yet visited
                for child_id in the_node.children:
                    if child_id not in yet_visited:
                        to_visit += [child_id]

                # We mark all his parents to visit if not yet visited
                for parent_id in the_node.parents:
                    if parent_id not in yet_visited:
                        to_visit += [parent_id]

            color += 1  # Next color

        return color, id_to_con

    def connected_components_list(self) -> List[OpenDigraph]:
        """ Decompose my self in independent Digraphs

        :return: list of independent digraphs
        """
        nb_con_com, dict_con_com = self.connected_components()
        digraph_list = [self.empty() for _ in range(nb_con_com)]

        for node_id in dict_con_com:
            digraph_list[dict_con_com[node_id]].node_map[node_id] = self.get_node_by_id(
                node_id).copy()

        for node_id in self.inputs_ids:
            digraph_list[dict_con_com[node_id]].inputs_ids += [node_id]

        for node_id in self.outputs_ids:
            digraph_list[dict_con_com[node_id]].add_output_id(node_id)

        for d in digraph_list:
            d._max = max(d.nodes_ids)

        return digraph_list

    # endregion

    # region Path, Depth, & Topological Sorting
    def dijkstra(self, src: Id, direction: int = None,
                 tgt: Id = None) -> Tuple[Dict[Id, int], Dict[Id, Id]]:
        """ Run Dijkstra Algorithm on the graph.

        :param src: Source Node Id
        :param direction: Direction used to select neighbours (-1 for
            parents, 1 for children, None for both)
        :param tgt: Target Node Id
        :return: Tuple of Distance Dict and Previous Node Dict """

        def get_neighbours(node_id: Id):
            """ Get neighbours of `node_id` with specified `direction` mode
            :param node_id: Node Used
            :return: Node's Neighbours """
            used_node = self.get_node_by_id(node_id)

            if direction is None:
                return used_node.parents + used_node.children
            elif direction == -1:
                return used_node.parents
            elif direction == 1:
                return used_node.children

        Q = [src]
        dist = {src: 0}
        prev = {}

        # Every node id of Q are in dist
        while Q:
            u = min(Q, key=lambda x: dist[x])
            Q.remove(u)
            if tgt == u:
                return dist, prev

            for v in get_neighbours(u):
                if v not in dist:
                    Q += [v]
                if v not in dist or dist[v] > dist[u] + 1:
                    dist[v] = dist[u] + 1
                    prev[v] = u
        return dist, prev

    @staticmethod
    def _extract_path(src: Id, tgt: Id, previous_node: Dict[Id, Id]) -> List[Id]:
        if tgt not in previous_node:
            raise RuntimeError(f"Unable to find a path from {src} to {tgt} !")

        current_node = tgt
        node_path = [tgt]

        while current_node != src:
            current_node = previous_node[current_node]
            node_path = [current_node] + node_path

        return node_path

    def shortest_path(self, src: Id, tgt: Id) -> Tuple[List[Id], int]:
        """ Return the shortest path from `src` to `tgt`
        
        :param src: Source Node 
        :param tgt: Target Node
        :return: Shortest Path """
        tmp = self._extract_path(src, tgt, self.dijkstra(src=src, tgt=tgt)[1])
        return tmp, len(tmp) - 1

    def common_ancestors(self, fst_node: Id, snd_node: Id) -> Dict[Id, Tuple[int, int]]:
        """ Common Ancestors of `fst_node` and `snd_node` with their respective distance

        :param fst_node: First Node Id
        :param snd_node: Second Node Id
        :return: Common Ancestors Dict """
        ancestors_fst_node, _ = self.dijkstra(src=fst_node, direction=-1)
        ancestors_snd_node, _ = self.dijkstra(src=snd_node, direction=-1)

        common_ancestors = {}

        largest_dict = ancestors_fst_node
        smallest_dict = ancestors_snd_node

        if len(smallest_dict) > len(largest_dict):
            largest_dict, smallest_dict = smallest_dict, largest_dict

        for node in smallest_dict:
            if node in largest_dict:
                common_ancestors[node] = ancestors_fst_node[node], ancestors_snd_node[node]

        return common_ancestors

    def topological_sort(self) -> Dict[int, List[Id]]:
        """ Maps depth to node class

        :return: Return Dictionary of Node Class mapped by their depth """
        g = self.copy()

        i = 0  # depth
        sorted_topological_class = {}

        while g.nodes:
            leaf_list_id = g.leaf_list(lower_leaf=False, node_list=False)

            if not leaf_list_id:
                raise RuntimeError("The graph is cyclic !")
            else:
                sorted_topological_class[i] = leaf_list_id
            g.remove_several_node_by_id(leaf_list_id)
            i += 1

        return sorted_topological_class

    def node_depth(self, node: Id) -> int:
        """ Return `node` depth in graph.

        :param node: Used Node (Id)
        :return: Node depth """

        if node not in self.nodes_ids:
            raise RuntimeError(f"Node {node} not in graph.")

        g = self.copy()

        i = 0  # depth
        leaf_list = g.leaf_list(False)

        while node not in leaf_list:
            if not leaf_list:  # if leaf_list is empty
                raise ValueError("The graph is cyclic !")
            i += 1
            g.remove_several_node_by_id(leaf_list)
            leaf_list = g.leaf_list(False)

        return i

    def graph_depth(self) -> int:
        """ Return Graph depth

        :return: Graph depth """
        return len(self.topological_sort()) - 1

    def dijkstra_inverse(self, src: Id, tgt: Id) -> Tuple[Dict[Id, int], Dict[Id, Id]]:
        """ Kind of the inverse of Dijkstra Algorithm... Compute the largest paths  from src to tgt

        :param src: Source Node
        :param tgt: Target Node
        :return: Tuple of Distance Dict and Previous Node Dict """
        l_i = self.topological_sort()

        k = self.node_depth(src)
        k_prime = self.node_depth(tgt)

        # Now, there is m (m=k_prime !) such as tgt \in dist[m]

        dist = {src: 0}
        prev = {}

        for node_class in l_i[k + 1:k_prime + 1].values():  # Works even if k_prime+1 >= graph_depth
            for node in node_class:
                parents_in_dist = set(self.get_node_by_id(node).parents_ids).intersection(set(dist))
                if parents_in_dist:  # A parent of node is in dist
                    next_node = max(parents_in_dist, key=lambda x: dist[x])
                    dist[node] = dist[next_node] + 1
                    prev[node] = next_node
                if node == tgt:
                    return dist, prev

    def longest_path(self, src: Id, tgt: Id) -> Tuple[List[Id], int]:
        """ Return the longest path from `src` to `tgt`

        :param src: Source Node
        :param tgt: Target Node
        :return: Longest Path, distance """

        tmp = self._extract_path(src, tgt, self.dijkstra_inverse(src=src, tgt=tgt)[1])
        return tmp, len(tmp) - 1

    # endregion
