from random import random, choice
from typing import List, NewType

IntMatrix = NewType('IntMatrix', List[List[int]])


def random_int_list(n: int, bound: int, number_generator=random) -> List[int]:
    """ Return a random made int list
    :param n: Number of element
    :param bound: Max bound
    :param number_generator: Random generator used
    :return: List of Int
    """
    return [int(number_generator() * (bound + 1)) for _ in range(n)]


def empty_matrix(n: int) -> IntMatrix:
    return [[0 for _ in range(n)] for _ in range(n)]


def print_matrix(matrix: IntMatrix):
    for line in matrix:
        for item in line:
            print(f"{item:^4}", end="")
        print()


def _null_diag(matrix: IntMatrix, null_diag: bool):
    """ Annihilate Matrix Diagonal
    :param matrix: Matrix used
    :param null_diag: Specify whether the diagonal is set null
    """
    if null_diag:
        for i in range(len(matrix)):
            matrix[i][i] = 0


def random_int_matrix(n: int, bound: int, number_generator=random,
                      null_diag: bool = True) -> IntMatrix:
    """Return a random made squared IntMatrix
    :param n: Width of the IntMatrix
    :param bound: Max bound
    :param number_generator: Random generator used
    :param null_diag: Specify whether the diagonal is null
    :return: IntMatrix
    """
    M = [random_int_list(n, bound, number_generator) for _ in range(n)]
    _null_diag(M, null_diag)

    return M


def random_symmetric_int_matrix(n: int, bound: int, number_generator=random,
                                null_diag: bool = True) -> IntMatrix:
    """Return a random made squared symmetric IntMatrix
    :param n: Width of the IntMatrix
    :param bound: Max bound
    :param number_generator: Random generator used
    :param null_diag: Specify whether the diagonal is null
    :return: IntMatrix
    """
    M = empty_matrix(n)
    for i in range(n):
        for j in range(i, n):
            M[j][i] = int(number_generator() * (bound + 1))
            M[i][j] = M[j][i]

    if null_diag:
        _null_diag(M, null_diag)

    return M


def random_oriented_int_matrix(n: int, bound: int, number_generator=random,
                               null_diag: bool = True) -> IntMatrix:
    """ Return a random made square oriented IntMatrix
    :param n: Width of the IntMatrix
    :param bound: Max bound
    :param number_generator: Random generator used
    :param null_diag: Specify whether the diagonal is null
    :return: IntMatrix
    """
    M = empty_matrix(n)
    for i in range(n):
        for j in range(i, n):
            if choice([True, False]):
                M[j][i] = int(number_generator() * (bound + 1))
            else:
                M[i][j] = int(number_generator() * (bound + 1))

    if null_diag:
        _null_diag(M, null_diag)

    return M


def random_triangular_int_matrix(n: int, bound: int, number_generator=random,
                                 null_diag: bool = True) -> IntMatrix:
    """ Return a random made square upper triangular IntMatrix
    :param n: Width of the IntMatrix
    :param bound: Max bound
    :param number_generator: Random generator used
    :param null_diag: Specify whether the diagonal is null
    :return: IntMatrix
    """
    M = empty_matrix(n)
    for i in range(n):
        for j in range(i, n):
            M[i][j] = int(number_generator() * (bound + 1))

    if null_diag:
        _null_diag(M, null_diag)

    return M
