from typing import List


class Parser:
    open_parenthesis = '('
    closed_parenthesis = ')'

    def __init__(self, only_split_with_space: bool = False):
        self._opps = {}
        self._other_tokens = [self.open_parenthesis, self.closed_parenthesis]
        self._only_split_with_space = only_split_with_space

    def add_operation(self, opp: str, unary=False):
        self._opps[opp] = unary

    @property
    def tokens(self):
        return self._other_tokens + list(self._opps)

    @staticmethod
    def _split_text(text: str, sep: str) -> List[str]:
        tmp = text.split(sep)
        pos = 0
        while pos < len(tmp) - 1:
            tmp.insert(pos + 1, sep)
            pos += 2

        return tmp

    def _split_tokens(self, text: str) -> List[str]:
        buf = text.split(' ')  # Use space in order to split tokens

        for sep in self._other_tokens:
            pos = 0
            while pos < len(buf):
                tmp = self._split_text(buf[pos], sep)
                buf = buf[:pos] + tmp + buf[pos + 1:]
                pos += len(tmp)

        if not self._only_split_with_space:
            for sep in self._opps:
                pos = 0
                while pos < len(buf):
                    tmp = self._split_text(buf[pos], sep)
                    buf = buf[:pos] + tmp + buf[pos + 1:]
                    pos += len(tmp)

        pos = 0
        while pos < len(buf):
            if buf[pos] == '':
                buf.pop(pos)
            else:
                pos += 1

        return buf

    def _find_close_parenthesis(self, open_position: int, token_list: List[str]) -> int:
        depth = 1
        pos = open_position + 1
        while depth > 0 and pos < len(token_list):
            if token_list[pos] == self.open_parenthesis:
                depth += 1
            elif token_list[pos] == self.closed_parenthesis:
                depth -= 1
            pos += 1

        if pos < len(token_list) or token_list[-1] == self.closed_parenthesis:
            return pos - 1
        else:
            raise RuntimeError("Opening Parenthesis without Closing one")

    def _organize_by_opp(self, token_list: List[str]):
        if not token_list:
            return []

        if len(token_list) == 1 and token_list[0] not in self.tokens:
            return token_list[0]

        members = {}  # position in string to terms
        operations = {}  # position in string -> [operator, unary opp ?]

        # Get Members & Operations
        pos = 0
        while pos < len(token_list):
            if token_list[pos] == self.open_parenthesis:  # if '(' -> begin of group
                other_parenthesis = self._find_close_parenthesis(pos, token_list)
                members[pos] = token_list[pos + 1:other_parenthesis]  # Parenthesis excluded
                pos = other_parenthesis
            elif token_list[pos] == self.closed_parenthesis:
                raise RuntimeError("Closed Parenthesis without Opening one")
            elif token_list[pos] in self._opps:  # Operation detected
                opp_token = token_list[pos]
                operations[pos] = [opp_token, self._opps[opp_token]]  # [opp, unary ?] like
            else:  # New single member
                members[pos] = [token_list[pos]]

            pos += 1

        # Group member by operation
        if not operations:
            if len(members) == 1:
                return self._organize_by_opp(members.popitem()[1])
            else:
                raise RuntimeError(f"Multiples terms ({members}) without any operation")
        elif len(operations) == 1 and operations[list(operations)[0]][1]:  # Only a unary operation
            opp_pos, opp_param = operations.popitem()

            if len(members) == 1:
                mem_pos, mem = members.popitem()
                if mem_pos > opp_pos:
                    return [opp_param[0], self._organize_by_opp(mem)]
                else:
                    raise RuntimeError("Wrong formatted unary operation (term before op ?)")
            else:
                raise RuntimeError("Wrong formatted unary operation (multiple terms)")
        else:  # multiple operations...
            # Stick unary opp to their members
            for opp_pos, opp_param in list(operations.items()):
                if opp_param[1]:  # if unary opp
                    if opp_pos + 1 not in members:
                        raise RuntimeError("Wrong formatted unary operation (no term after op)")
                    else:
                        members[opp_pos + 1] = [opp_param[0], self.open_parenthesis] + \
                                               members[opp_pos + 1] + [self.closed_parenthesis]
                        operations.pop(opp_pos)

            set_off_opp = set([ope for ope, _ in operations.values()])

            if len(set_off_opp) > 1:
                raise RuntimeError("Multiple operation at the same level (ambiguity)")
            elif len(set_off_opp) == 0:
                raise RuntimeError("Multiples terms without binary operator")

            # Now, only one operator is in operations
            # Let's check positions of terms and operations

            if len(members) != len(operations) + 1:
                raise RuntimeError("Too many terms or too many operations in list")

            member_pos = sorted(list(members))
            operations_pos = sorted(list(operations))

            for i in range(len(operations_pos)):
                if not (member_pos[i] < operations_pos[i] < member_pos[i + 1]):
                    raise RuntimeError("Wrong formatted multi-binary operation.")

            return [operations.popitem()[1][0]] + [self._organize_by_opp(mem) for mem in
                                                   members.values()]

    def parse(self, text):
        return self._organize_by_opp(self._split_tokens(text))
